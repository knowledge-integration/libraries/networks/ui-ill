import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { useIntlKeyStore } from '@k-int/stripes-kint-components';
import {
  Layout,
} from '@folio/stripes/components';

import PatronRequestsRoute from './routes/PatronRequestsRoute';
import CreateEditRoute from './routes/CreateEditRoute';
import ViewRoute from './routes/ViewRoute';
import PullSlipRoute from './routes/PullSlipRoute';
import { EditPullslipNotification } from './settings/pullslipNotifications';
import IllSettings from './settings';
import InstitutionsRoute from './routes/System/Institutions/InstitutionsRoute';
import InstitutionGroupsRoute from './routes/System/Institutions/InstitutionGroupsRoute';
import InstitutionUsersRoute from './routes/System/Institutions/InstitutionUsersRoute';
import SharedIndexQueryRoute from './routes/System/SharedIndex/SharedIndexQueryRoute';
import css from './index.css';

import {
  ADMIN_INSTITUTION_GROUPS,
  ADMIN_INSTITUTION_USERS,
  ADMIN_INSTITUTIONS,
  BASE_ADMIN_URL,
  BASE_REQUESTS_URL,
  REQUESTS_BORROWER,
  REQUESTS_CONTEXT_REQUESTS,
  REQUESTS_CONTEXT_REQUESTS_VIEW,
  REQUESTS_SHARED_INDEX,
  REQUESTS_WITH_CONTEXT
} from './constants/urls';
import { TopLevelRoutingButtons } from './components/RoutingButtonGroups';

const ResourceSharing = (props) => {
  const {
    actAs,
    match: { path },
    location: { search }
  } = props;

  // stripes-kint-components no longer contains translations for its strings and needs to know where to look
  const addKey = useIntlKeyStore(state => state.addKey);
  addKey('stripes-ill');

  if (actAs === 'settings') {
    return <IllSettings {...props} />;
  }

  return (
    <Layout className={css.container}>
      <Layout className={`${css.header} padding-top-gutter padding-start-gutter padding-end-gutter`}>
        <TopLevelRoutingButtons />
      </Layout>
      <Layout className={css.body}>
        <Switch>
          <Redirect
            exact
            from={path}
            to={`${BASE_REQUESTS_URL}${search}`}
          />

          <Redirect
            exact
            from={BASE_REQUESTS_URL}
            to={`${REQUESTS_BORROWER}${search}`}
          />

          {/* First check whether we are on the "special" shared index page */}
          <Route path={REQUESTS_SHARED_INDEX} render={(p) => <SharedIndexQueryRoute {...p} />} />

          {/* If not, redirect <context> to <context>/requests */}
          <Redirect
            exact
            from={REQUESTS_WITH_CONTEXT}
            to={`${REQUESTS_CONTEXT_REQUESTS}${search}`}
          />

          <Route path={`${REQUESTS_CONTEXT_REQUESTS}/create`} component={CreateEditRoute} />
          <Route path={`${REQUESTS_CONTEXT_REQUESTS}/edit/:id`} component={CreateEditRoute} />

          {/* Special case for pullslip management, else route to "viewMode" (Flow vs details) */}
          <Route path={`${REQUESTS_CONTEXT_REQUESTS_VIEW}/pullslip`} component={PullSlipRoute} />
          <Route path={`${REQUESTS_CONTEXT_REQUESTS}/batch/:batchId/pullslip`} component={PullSlipRoute} />
          {/* Double check these belong in "requests" side */}
          <Route path={`${REQUESTS_WITH_CONTEXT}/pullslip-notifications`} component={EditPullslipNotification} />
          <Route path={`${REQUESTS_WITH_CONTEXT}/pullslip-notifications/:id/edit`} component={EditPullslipNotification} />

          <Redirect
            exact
            from={`${REQUESTS_CONTEXT_REQUESTS_VIEW}`}
            to={`${REQUESTS_CONTEXT_REQUESTS_VIEW}/flow${search}`}
          />

          <Route path={`${REQUESTS_CONTEXT_REQUESTS_VIEW}/:viewMode`} component={ViewRoute} />
          {/* Not entirely sure what this routing means right now */}
          <Route path={`${REQUESTS_CONTEXT_REQUESTS}/:action?`} render={(p) => <PatronRequestsRoute {...p} />} />

          {/* We now enter the "admin" side of routing */}
          <Redirect
            exact
            from={BASE_ADMIN_URL}
            to={`${ADMIN_INSTITUTIONS}${search}`}
          />
          <Route path={ADMIN_INSTITUTIONS} render={(p) => <InstitutionsRoute {...p} />} />
          <Route path={ADMIN_INSTITUTION_GROUPS} render={(p) => <InstitutionGroupsRoute {...p} />} />
          <Route path={ADMIN_INSTITUTION_USERS} render={(p) => <InstitutionUsersRoute {...p} />} />
        </Switch>
      </Layout>
    </Layout>
  );
};

export default ResourceSharing;

// FIXME not 100% sure about exporting these things from `ui-ill` to be honest.
//  Feels like these could go in stripes-ill, or we could roll all of this in
//  without anything shared since we're aiming for single UI app.
export * from './constants/endpoints';
export * from './constants/urls';

export { REQUEST_ACTIONS } from './constants/requestActions';

export { DeveloperJson } from './components/DeveloperJson';
