import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useMutation, useQueryClient } from 'react-query';
import { Field } from 'react-final-form';
import { FORM_ERROR } from 'final-form';
import { useHistory } from 'react-router';

import { Button, Pane, TextField } from '@folio/stripes/components';
import { useOkapiKy } from '@folio/stripes/core';
import { ActionList, FormModal } from '@k-int/stripes-kint-components';
import { useIntlCallout } from '@k-int/stripes-ill';
import ShelvingLocationSiteForm from './ShelvingLocationSiteForm';
import {
  HOST_LMS_LOCATIONS_ENDPOINT,
  SHELVING_LOCATION_SITES_ENDPOINT,
} from '../../constants/endpoints';

const ShelvingLocationSites = ({ location }) => {
  const ky = useOkapiKy();
  const queryClient = useQueryClient();
  const sendCallout = useIntlCallout();
  const history = useHistory();

  const [formModal, setFormModal] = useState(false);

  const { mutateAsync: putSite } = useMutation(
    ['ui-ill-ui', 'putShelvingLocationSite'],
    async (data) => {
      await ky.put(`${SHELVING_LOCATION_SITES_ENDPOINT}/${data.id}`, { json: data }).json();
      queryClient.invalidateQueries(SHELVING_LOCATION_SITES_ENDPOINT);
      queryClient.invalidateQueries(HOST_LMS_LOCATIONS_ENDPOINT);
    }
  );

  const { mutateAsync: postSite } = useMutation(
    ['ui-ill-ui', 'postShelvingLocationSite'],
    async (data) => {
      await ky.post(SHELVING_LOCATION_SITES_ENDPOINT, { json: data }).json();
      queryClient.invalidateQueries(SHELVING_LOCATION_SITES_ENDPOINT);
      queryClient.invalidateQueries(HOST_LMS_LOCATIONS_ENDPOINT);
    }
  );

  const { mutateAsync: deleteSite } = useMutation(
    ['ui-ill-ui', 'deleteShelvingLocationSite'],
    async (data) => {
      await ky.delete(`${SHELVING_LOCATION_SITES_ENDPOINT}/${data.id}`, { json: data })
        .catch(error => {
          error.response.json()
            .then(resp => {
              // This simultaneously checks the error type and that we have a sensible array of linked ids
              if (resp.linkedPatronRequests?.length) {
                sendCallout('ui-ill-ui.settings.lmsloc.linkedPRs', 'error', { prs: resp.linkedPatronRequests?.join(', ') });
              }
            });
        });
      queryClient.invalidateQueries(SHELVING_LOCATION_SITES_ENDPOINT);
      queryClient.invalidateQueries(HOST_LMS_LOCATIONS_ENDPOINT);
    }
  );


  if (!Array.isArray(location?.sites)) return null;
  const sites = location.sites.map(site => ({
    ...site,
    code: site.shelvingLocation.code,
    name: site.shelvingLocation.name,
  }));

  const actionAssigner = () => {
    return ([
      {
        name: 'edit',
        callback: (data) => putSite(data),
        label: <FormattedMessage id="ui-ill-ui.edit" />,
        icon: 'edit',
      },
      {
        name: 'delete',
        callback: (data) => deleteSite(data),
        label: <FormattedMessage id="ui-ill-ui.delete" />,
        icon: 'trash',
      },
    ]);
  };

  const fieldComponents = {
    supplyPreference: ({ ...fieldProps }) => {
      return (
        <Field
          {...fieldProps}
          component={TextField}
          type="number"
          fullWidth
          marginBottom0
          parse={v => v}
        />
      );
    }
  };

  return (
    <>
      <Pane
        defaultWidth="fill"
        dismissible
        onClose={() => history.push({ search: '' })}
        lastMenu={
          <Button
            marginBottom0
            onClick={() => setFormModal(true)}
          >
            <FormattedMessage id="ui-ill-ui.create" />
          </Button>
        }
        paneTitle={<FormattedMessage id="ui-ill-ui.settings.lmsloc.overridesFor" values={{ location: location.name }} />}
      >
        <ActionList
          actionAssigner={actionAssigner}
          columnMapping={{
            name: <FormattedMessage id="ui-ill-ui.settings.lmsshlv.shelvingLocation" />,
            code: <FormattedMessage id="ui-ill-ui.settings.lmsshlv.code" />,
            supplyPreference: <FormattedMessage id="ui-ill-ui.settings.lmsloc.supplyPreference" />,
          }}
          contentData={sites}
          editableFields={{
            code: () => false,
            name: () => false,
          }}
          fieldComponents={fieldComponents}
          hideCreateButton
          visibleFields={['name', 'code', 'supplyPreference']}
        />
      </Pane>
      <FormModal
        onSubmit={async (data, form) => {
          try {
            await postSite({ ...data, location: location.id });
            form.restart();
            setFormModal(false);
            return undefined;
          } catch (e) {
            const res = await e?.response?.json();
            return { [FORM_ERROR]: res.message ?? e.message };
          }
        }}
        modalProps={{
          onClose: () => setFormModal(false),
          open: formModal,
          size: 'small',
          label: <FormattedMessage id="ui-ill-ui.settings.lmsloc.createOverrideFor" values={{ location: location.name }} />,
        }}
      >
        <ShelvingLocationSiteForm location={location} />
      </FormModal>
    </>
  );
};

export default ShelvingLocationSites;
