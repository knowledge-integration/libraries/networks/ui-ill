import React from 'react';
import { useIntl } from 'react-intl';

import tokens from './tokens';
import TokensList from './TokensList';

import Templates from '../templates';

const Notices = () => {
  const intl = useIntl();
  return (
    <Templates
      context="noticeTemplate"
      label={intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.notices' })}
      permissions={{
        put: 'ui-ill-ui.settings.notices',
        post: 'ui-ill-ui.settings.notices',
        delete: 'ui-ill-ui.settings.notices',
      }}
      templateContextLabel={intl.formatMessage({ id: 'ui-ill-ui.settings.templates.noticeTemplate' })?.toLowerCase()}
      tokens={tokens}
      tokensList={TokensList}
    />
  );
};

export default Notices;
