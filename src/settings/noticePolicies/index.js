import React from 'react';
import PropTypes from 'prop-types';
import { sortBy } from 'lodash';
import { EntryManager } from '@folio/stripes/smart-components';
import { stripesConnect } from '@folio/stripes/core';

import NoticePolicyDetail from './NoticePolicyDetail';
import NoticePolicyForm from './NoticePolicyForm';
import { NOTICE_POLICIES_ENDPOINT } from '../../constants/endpoints';

const NoticePolicies = (props) => (
  <EntryManager
    {...props}
    parentMutator={props.mutator}
    entryList={sortBy((props.resources.entries || {}).records || [], ['name'])}
    detailComponent={NoticePolicyDetail}
    paneTitle={props.label}
    entryLabel={props.label}
    entryFormComponent={NoticePolicyForm}
    defaultEntry={{
      active: true,
    }}
    nameKey="name"
    permissions={{
      put: 'ui-ill-ui.settings.notices',
      post: 'ui-ill-ui.settings.notices',
      delete: 'ui-ill-ui.settings.notices',
    }}
    enableDetailsActionMenu
    editElement="both"
  />
);

NoticePolicies.manifest = Object.freeze({
  entries: {
    type: 'okapi',
    path: NOTICE_POLICIES_ENDPOINT,
    max: 1000,
  }
});

NoticePolicies.propTypes = {
  mutator: PropTypes.object,
  resources: PropTypes.object,
  label: PropTypes.string,
};

export default stripesConnect(NoticePolicies);
