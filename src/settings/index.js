import { Route } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import { useSettings } from '@k-int/stripes-kint-components';

import ShelvingLocationSites from './HostLMSLocations/ShelvingLocationSites';
import {
  ViewPullslipNotification,
  EditPullslipNotification,
  CreatePullslipNotification
} from './pullslipNotifications';

import {
  REFDATA_ENDPOINT,
  SETTINGS_INSTITUTION_ENDPOINT, SETTINGS_SYSTEM_ENDPOINT,
  TEMPLATES_ENDPOINT
} from '../constants/endpoints';
import useILLSettingsPersistentPages from './useILLSettingsPersistentPages';
import { SYSTEM_AREA_SETTINGS } from '../components/System/SystemAreas';

const IllSettings = (props) => {
  const { match } = props;

  const { persistentPages } = useILLSettingsPersistentPages();

  const { isLoading, SettingsComponent } = useSettings({
    intlKey: 'ui-ill-ui',
    refdataEndpoint: REFDATA_ENDPOINT,
    sections: [
      {
        label: <FormattedMessage id="ui-ill-ui.settings" />,
        dynamicPageExclusions: ['pullslipConfiguration', 'pullslipTemplateConfig'],
        persistentPages,
      },
      {
        label: <FormattedMessage id="ui-ill-ui.system.settings" />,
        settingEndpoint: SETTINGS_SYSTEM_ENDPOINT,
        sectionPermission: SYSTEM_AREA_SETTINGS.permission,
        sectionRoute: 'system'
      }
    ],
    settingEndpoint: SETTINGS_INSTITUTION_ENDPOINT,
    templateEndpoint: TEMPLATES_ENDPOINT
  });

  const additionalRoutes = [
    <Route
      key="pullslip-notifications/new"
      path={`${match.path}/pullslip-notifications/new`}
      component={CreatePullslipNotification}
    />,
    <Route
      key="pullslip-notifications/:id/edit"
      path={`${match.path}/pullslip-notifications/:id/edit`}
      component={EditPullslipNotification}
    />,
    <Route
      key="pullslip-notifications/:id"
      path={`${match.path}/pullslip-notifications/:id`}
      component={ViewPullslipNotification}
    />,
    <Route
      key="lmslocations/:id/shelvingsites"
      path={`${match.path}/lmslocations/:id/shelvingsites`}
      component={ShelvingLocationSites}
    />
  ];

  if (isLoading) {
    return null;
  }

  return (
    <SettingsComponent
      {...props}
      additionalRoutes={additionalRoutes}
    />
  );
};

export default IllSettings;
