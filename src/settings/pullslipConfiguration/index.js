import { Pane, Paneset } from '@folio/stripes/components';
import { useOkapiQuery } from '@k-int/stripes-ill';
import { FormattedMessage } from 'react-intl';
import FileSetting from './FileSetting';
import ReportSetting from './ReportSetting';
import { SETTINGS_INSTITUTION_ENDPOINT } from '../../constants/endpoints';

const PullslipConfiguration = () => {
  const { data: settings, isSuccess: settingsLoaded } = useOkapiQuery(SETTINGS_INSTITUTION_ENDPOINT, { searchParams: { filters: 'section==pullslipConfiguration' } });
  if (!settingsLoaded) return null;
  return (
    <Paneset>
      <Pane
        defaultWidth="fill"
        paneTitle={<FormattedMessage id="ui-ill-ui.settings.settingsSection.pullslipConfiguration" />}
      >
        <FileSetting setting={settings.filter(s => s.key === 'pull_slip_logo_id')[0]} fileType="LOGO" />
        <ReportSetting setting={settings.filter(s => s.key === 'pull_slip_report_id')[0]} fileType="REPORT_DEFINITION" />
      </Pane>
    </Paneset>
  );
};

export default PullslipConfiguration;
