import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { toCamelCase } from '@k-int/stripes-kint-components';
import { Button, Card, Layout } from '@folio/stripes/components';
import { FileUploader } from '@folio/stripes-data-transfer-components';
import { useOkapiKy } from '@folio/stripes/core';
import { useQueryClient } from 'react-query';
import { useIntlCallout } from '@k-int/stripes-ill';
import {
  FILE_DEFINITION_FILE_UPLOAD_ENDPOINT,
  SETTINGS_INSTITUTION_ENDPOINT
} from '../../constants/endpoints';

const FileSetting = ({ setting, fileType }) => {
  const okapiKy = useOkapiKy();
  const queryClient = useQueryClient();
  const sendCallout = useIntlCallout();
  const [editing, setEditing] = useState(false);

  const invalidate = () => queryClient.invalidateQueries(SETTINGS_INSTITUTION_ENDPOINT, { filters: 'section==pullslipConfiguration' });

  const upload = async (dropped) => {
    const formData = new FormData();
    formData.set('file', dropped[0]);
    formData.set('fileType', fileType);
    formData.set('description', dropped[0]?.name);
    const uploadResult = await okapiKy.post(FILE_DEFINITION_FILE_UPLOAD_ENDPOINT, { body: formData }).json();
    await okapiKy.put(`${SETTINGS_INSTITUTION_ENDPOINT}/${setting.id}`, { json: { value: uploadResult.id } });
    await invalidate();
    setEditing(false);
    sendCallout('ui-ill-ui.settings.fileSetting.success', 'success');
  };

  const clearValue = async () => {
    await okapiKy.put(`${SETTINGS_INSTITUTION_ENDPOINT}/${setting.id}`, { json: { value: '' } });
    await invalidate();
    setEditing(false);
  };

  const onDrop = (dropped) => {
    upload(dropped).catch(async e => {
      const res = await e?.response?.json();
      const errMsg = res.error ?? e.message;
      sendCallout('ui-ill-ui.settings.fileSetting.error', 'error', { errMsg });
    });
  };

  return (
    <Card
      headerStart={<FormattedMessage id={`ui-ill-ui.settings.${toCamelCase(setting.section)}.${toCamelCase(setting.key)}`} />}
      headerEnd={
        <Button
          marginBottom0
          disabled={editing}
          onClick={() => {
            setEditing(true);
          }}
        >
          <FormattedMessage id="stripes-ill.settings.edit" />
        </Button>
      }
      roundedBorder
    >
      {!editing && (setting.value
        ? <FormattedMessage id="ui-ill-ui.settings.fileSetting.valueSet" />
        : <FormattedMessage id="stripes-ill.settings.noCurrentValue" />
      )}
      {editing &&
        <>
          <FileUploader
            title={<FormattedMessage id="ui-ill-ui.settings.fileSetting.drop" />}
            uploadButtonText={<FormattedMessage id="ui-ill-ui.settings.fileSetting.button" />}
            onDrop={onDrop}
          />
          <Layout className="padding-top-gutter display-flex justify-end">
            <Button
              marginBottom0
              onClick={clearValue}
            >
              <FormattedMessage id="ui-ill-ui.settings.fileSetting.reset" />
            </Button>
            <Button
              marginBottom0
              onClick={() => {
                setEditing(false);
              }}
            >
              <FormattedMessage id="stripes-ill.cancel" />
            </Button>
          </Layout>
        </>
      }
    </Card>
  );
};

export default FileSetting;
