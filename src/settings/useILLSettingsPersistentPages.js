import { useIntl } from 'react-intl';
import { CustomISO18626 } from './settingsComponents';
import Notices from './notices';
import OtherSettings from './OtherSettings';
import PullslipConfiguration from './pullslipConfiguration';
import PullslipTemplates from './pullslipTemplates';
import NoticePolicies from './noticePolicies';
import { PullslipNotifications } from './pullslipNotifications';
import HostLMSLocations from './HostLMSLocations';
import HostLMSItemLoanPolicies from './HostLMSItemLoanPolicies';
import HostLMSPatronProfiles from './HostLMSPatronProfiles';
import HostLMSShelvingLocations from './HostLMSShelvingLocations';
import Services from './Services/Services';
import Tags from './Tags/Tags';

export default () => {
  const intl = useIntl();

  const persistentPages = [
    {
      route: 'CustomISO18626Settings',
      id: 'iso18626',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.iso18626' }),
      component: CustomISO18626
    },
    {
      route: 'notices',
      id: 'notices',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.notices' }),
      component: Notices,
      perm: 'ui-ill-ui.settings.notices',
    },
    {
      route: 'other',
      id: 'other',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.other' }),
      component: OtherSettings
    },
    {
      route: 'pullslipConfiguration',
      id: 'pullslipConfiguration',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.pullslipConfiguration' }),
      component: PullslipConfiguration,
      perm: 'ui-ill-ui.settings.pullslip-notifications',
    },
    {
      route: 'pullslipTemplates',
      id: 'pullslipTemplates',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.pullslipTemplates' }),
      component: PullslipTemplates,
      perm: 'ui-ill-ui.settings.pullslip-notifications',
    },
    {
      route: 'notice-policies',
      id: 'noticePolicies',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.noticePolicies' }),
      component: NoticePolicies,
      perm: 'ui-ill-ui.settings.notices',
    },
    {
      route: 'pullslip-notifications',
      id: 'pullslipNotifications',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.pullslipNotifications' }),
      component: PullslipNotifications,
      perm: 'ui-ill-ui.settings.pullslip-notifications',
    },
    {
      route: 'lmslocations',
      id: 'hostLMSLocations',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.hostLMSLocations' }),
      component: HostLMSLocations,
      perm: 'ui-ill-ui.settings.hostlmslocations',
    },
    {
      route: 'lmspolicies',
      id: 'hostLMSItemLoanPolicies',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.hostLMSItemLoanPolicies' }),
      component: HostLMSItemLoanPolicies,
      perm: 'ui-ill-ui.settings.hostlmslocations',
    },
    {
      route: 'lmsprofiles',
      id: 'hostLMSPatronProfiles',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.hostLMSPatronProfiles' }),
      component: HostLMSPatronProfiles,
      perm: 'ui-ill-ui.settings.hostlmslocations',
    },
    {
      route: 'lmsshelving',
      id: 'hostLMSShelvingLocations',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.hostLMSShelvingLocations' }),
      component: HostLMSShelvingLocations,
      perm: 'ui-ill-ui.settings.hostlmslocations',
    },
    {
      route: 'services',
      id: 'services',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.services' }),
      component: Services,
    },
    {
      route: 'tags',
      id: 'tags',
      label: intl.formatMessage({ id: 'ui-ill-ui.settings.tags' }),
      component: Tags,
    },
  ];

  return {
    persistentPages,
  };
};
