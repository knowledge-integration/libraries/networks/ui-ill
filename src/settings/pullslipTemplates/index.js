import React from 'react';
import { useIntl } from 'react-intl';

import tokens from './tokens';
import TokensList from './TokensList';

import Templates from '../templates';

const PullslipTemplates = () => {
  const intl = useIntl();
  return (
    <Templates
      context="pullslipTemplate"
      label={intl.formatMessage({ id: 'ui-ill-ui.settings.settingsSection.pullslipTemplates' })}
      permissions={{
        put: 'ui-ill-ui.settings.pullslip-notifications',
        post: 'ui-ill-ui.settings.pullslip-notifications',
        delete: 'ui-ill-ui.settings.pullslip-notifications',
      }}
      templateContextLabel={intl.formatMessage({ id: 'ui-ill-ui.settings.templates.pullslipTemplate' })?.toLowerCase()}
      tokens={tokens}
      tokensList={TokensList}
    />
  );
};

export default PullslipTemplates;
