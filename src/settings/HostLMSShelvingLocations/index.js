import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useMutation, useQueryClient } from 'react-query';
import { Field } from 'react-final-form';
import { FORM_ERROR } from 'final-form';

import { Button, Pane, TextField } from '@folio/stripes/components';
import { useOkapiKy } from '@folio/stripes/core';
import { ActionList, FormModal, generateKiwtQuery } from '@k-int/stripes-kint-components';
import { useOkapiQuery, useIntlCallout } from '@k-int/stripes-ill';
import ShelvingLocationForm from './ShelvingLocationForm';
import {
  SHELVING_LOCATIONS_ENDPOINT,
} from '../../constants/endpoints';

const HostLMSShelvingLocations = () => {
  const ky = useOkapiKy();
  const queryClient = useQueryClient();
  const sendCallout = useIntlCallout();

  const [formModal, setFormModal] = useState(false);

  const { data: locations } = useOkapiQuery(SHELVING_LOCATIONS_ENDPOINT, {
    searchParams: generateKiwtQuery({ sort: [{ path: 'name' }], stats: false, max: 1000 }, {}),
  });

  const { mutateAsync: putLocation } = useMutation(
    ['ui-ill-ui', 'putShelvingLocation'],
    async (data) => {
      await ky.put(`${SHELVING_LOCATIONS_ENDPOINT}/${data.id}`, { json: data }).json();
      queryClient.invalidateQueries(SHELVING_LOCATIONS_ENDPOINT);
    }
  );

  const { mutateAsync: postLocation } = useMutation(
    ['ui-ill-ui', 'postShelvingLocation'],
    async (data) => {
      await ky.post(SHELVING_LOCATIONS_ENDPOINT, { json: data }).json();
      queryClient.invalidateQueries(SHELVING_LOCATIONS_ENDPOINT);
    }
  );

  const { mutateAsync: deleteLocation } = useMutation(
    ['ui-ill-ui', 'deleteShelvingLocation'],
    async (data) => {
      await ky.delete(`${SHELVING_LOCATIONS_ENDPOINT}/${data.id}`, { json: data })
        .catch(error => {
          error.response.json()
            .then(resp => {
              // This simultaneously checks the error type and that we have a sensible array of linked ids
              if (resp.linkedPatronRequests?.length) {
                sendCallout('ui-ill-ui.settings.lmsloc.linkedPRs', 'error', { prs: resp.linkedPatronRequests?.join(', ') });
              }
            });
        });
      queryClient.invalidateQueries(SHELVING_LOCATIONS_ENDPOINT);
    }
  );

  const actionAssigner = () => {
    return ([
      {
        name: 'edit',
        callback: (data) => putLocation(data),
        label: <FormattedMessage id="ui-ill-ui.edit" />,
        icon: 'edit',
      },
      {
        name: 'delete',
        callback: (data) => deleteLocation(data),
        label: <FormattedMessage id="ui-ill-ui.delete" />,
        icon: 'trash',
      },
    ]);
  };

  const fieldComponents = {
    supplyPreference: ({ ...fieldProps }) => {
      return (
        <Field
          {...fieldProps}
          component={TextField}
          type="number"
          fullWidth
          marginBottom0
          parse={v => v}
        />
      );
    }
  };

  return (
    <>
      <Pane
        defaultWidth="fill"
        lastMenu={
          <Button
            marginBottom0
            onClick={() => setFormModal(true)}
          >
            <FormattedMessage id="ui-ill-ui.create" />
          </Button>
        }
        paneTitle={<FormattedMessage id="ui-ill-ui.settings.settingsSection.hostLMSShelvingLocations" />}
      >
        <ActionList
          actionAssigner={actionAssigner}
          columnMapping={{
            name: <FormattedMessage id="ui-ill-ui.settings.lmsshlv.shelvingLocation" />,
            code: <FormattedMessage id="ui-ill-ui.settings.lmsshlv.code" />,
            supplyPreference: <FormattedMessage id="ui-ill-ui.settings.lmsloc.supplyPreference" />,
          }}
          contentData={locations}
          editableFields={{
            code: () => false
          }}
          fieldComponents={fieldComponents}
          hideCreateButton
          visibleFields={['name', 'code', 'supplyPreference']}
        />
      </Pane>
      <FormModal
        onSubmit={async (data, form) => {
          try {
            await postLocation(data);
            form.restart();
            setFormModal(false);
            return undefined;
          } catch (e) {
            const res = await e?.response?.json();
            let message = res.message;
            if (message != null) {
              if (message.endsWith('must be unique')) {
                message = <FormattedMessage id="ui-ill-ui.settings.lmsshlv.alreadyExists" />;
              }
            }
            return { [FORM_ERROR]: message ?? e.message };
          }
        }}
        modalProps={{
          onClose: () => setFormModal(false),
          open: formModal,
          size: 'small',
          label: <FormattedMessage id="ui-ill-ui.settings.lmsshlv.createNew" />,
        }}
      >
        <ShelvingLocationForm />
      </FormModal>
    </>
  );
};

export default HostLMSShelvingLocations;
