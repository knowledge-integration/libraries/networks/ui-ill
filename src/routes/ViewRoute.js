import { useCallback, useEffect, useMemo } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import _ from 'lodash';
import { useStripes } from '@folio/stripes/core';

import { PersistedPaneset } from '@folio/stripes/smart-components';
import { Button, ButtonGroup, Icon, Layout, Pane, PaneMenu } from '@folio/stripes/components';
import { upNLevels, useCloseDirect, usePerformAction, useOkapiQuery } from '@k-int/stripes-ill';

import renderNamedWithProps from '../util/renderNamedWithProps';
import { MessageModalProvider } from '../components/MessageModalState';
import * as modals from '../components/Flow/modals';
import { actionsForRequest, excludeRemote } from '../components/Flow/actionsByState';
import { ManualClose, PrintPullSlip } from '../components/Flow/moreActions';
import FlowRoute from './FlowRoute';
import ViewPatronRequest from '../components/ViewPatronRequest';
import ViewMessageBanners from '../components/ViewMessageBanners';
import useRSHelperApp from '../components/useRSHelperApp';
import useChatActions from '../components/chat/useChatActions';
import { PATRON_REQUESTS_ENDPOINT } from '../constants/endpoints';

import css from './ViewRoute.css';
import { useRequestContext } from '../hooks';

const subheading = (req, id) => {
  if (!req || id !== req.id) return undefined;
  const title = _.get(req, 'title');
  if (!title) return undefined;
  const requester = _.get(req, 'resolvedRequester.owner.slug', '');
  if (!requester) return title;
  const supplier = _.get(req, 'resolvedSupplier.owner.slug', '');
  return `${title} · ${requester} → ${supplier}`;
};

const ViewRoute = ({
  location,
  location: { pathname },
  match: {
    params: { id, viewMode } = {},
    path
  } = {}
}) => {
  const intl = useIntl();
  const stripes = useStripes();
  const { ChatButton, HelperComponent, TagButton, isOpen } = useRSHelperApp();

  const { isBorrower, requestContext } = useRequestContext();

  const performAction = usePerformAction(id);
  const { handleMarkAllRead } = useChatActions(id);
  const close = useCloseDirect(upNLevels(location, 3));

  // Fetch the request
  const {
    data: request = {},
    isSuccess: hasRequestLoaded,
    isError: httpError
  } = useOkapiQuery(
    `${PATRON_REQUESTS_ENDPOINT}/${id}`,
    { staleTime: 2 * 60 * 1000, notifyOnChangeProps: 'tracked', useErrorBoundary: false }
  );

  // Did we hit an error loading the request
  if (httpError) {
    close();
  }

  /* On mount ONLY we want to check if the helper is open, and if so then mark all messages as read.
   * If this useEffect is handed dependencies handleMarkAllRead and isOpen then it will infinitely loop,
   */

  // This could maybe be solved by memoizing isOpen within useHelperApp?

  useEffect(() => {
    if (isOpen('chat')) {
      handleMarkAllRead(true, true);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const paneButtons = () => {
    return (
      <PaneMenu>
        {request?.resolvedSupplier &&
          <ChatButton
            request={request}
            onClick={({ open }) => {
              // This case handles the marking of messages as read when the chat pane opens for the first time within the view
              if (!open) {
                handleMarkAllRead(true, true);
              }
            }}
          />
        }
        <TagButton request={request} />
      </PaneMenu>
    );
  };

  // Rethink this a little, refactored to avoid stupid routes within routes
  const renderContents = useCallback(() => {
    if (viewMode === 'flow') {
      return <FlowRoute request={request} performAction={performAction} />;
    }
    return <ViewPatronRequest record={request} />;
  }, [viewMode, performAction, request]);

  // This is a little iffy... The flow/details redirect should work better than this
  const basePath = useMemo(() => {
    return path
      .replace(':id', id)
      .replace(':requestContext', requestContext)
      .replace('/:viewMode', '');
  }, [id, path, requestContext]);

  if (!hasRequestLoaded) return null;
  const forCurrent = actionsForRequest(request);

  return (
    <>
      <PersistedPaneset
        appId="@k-int/ill-ui"
        id="ill-ui/request-view-paneset"
      >
        <Pane
          actionMenu={() => (
            <>
              {
                isBorrower && stripes.hasPerm('ui-ill-request.edit') && (
                  <Button buttonStyle="dropdownItem" to={`/requests/edit/${id}`} id="clickable-edit-patronrequest">
                    <Icon icon="edit">
                      <FormattedMessage id="ui-ill-ui.edit" />
                    </Icon>
                  </Button>
                )
              }
              {request?.validActions?.includes('manualClose') &&
                <ManualClose />
              }
              <PrintPullSlip />
            </>
          )}
          centerContent
          defaultWidth="fill"
          dismissible
          id="request-view-pane"
          lastMenu={paneButtons()}
          onClose={close}
          padContent={false}
          paneSub={subheading(request, id)}
          paneTitle={intl.formatMessage({ id: 'ui-ill-ui.view.title' }, { id: request.hrid })}
          subheader={
            <Layout
              className={`${css.tabContainer} flex centerContent flex-align-items-center full padding-start-gutter padding-end-gutter`}
            >
              <ButtonGroup>
                <Button
                  marginBottom0
                  to={`${basePath}/flow${location.search}`}
                  buttonStyle={pathname.includes('/flow') ? 'primary' : 'default'}
                  replace
                >
                  <FormattedMessage id="ui-ill-ui.flow.flow" />
                </Button>
                <Button
                  marginBottom0
                  to={`${basePath}/details${location.search}`}
                  buttonStyle={pathname.includes('/details') ? 'primary' : 'default'}
                  replace
                >
                  <FormattedMessage id="ui-ill-ui.flow.details" />
                </Button>
              </ButtonGroup>
            </Layout>
          }
        >
          <ViewMessageBanners request={request} />
          {renderContents()}
        </Pane>
        <HelperComponent {...{ request, isOpen }} />
      </PersistedPaneset>
      {/* Render modals that correspond to available actions, adding back excluded actions */}
      {renderNamedWithProps(
        forCurrent.primaryAction && !forCurrent.moreActions.includes(forCurrent.primaryAction)
          ? [forCurrent.primaryAction, ...forCurrent.moreActions, ...excludeRemote]
          : [...forCurrent.moreActions, ...excludeRemote],
        modals,
        { request, performAction }
      )}
    </>
  );
};

const ViewRouteWithContext = props => (
  <MessageModalProvider>
    <ViewRoute {...props} />
  </MessageModalProvider>
);

export default ViewRouteWithContext;
