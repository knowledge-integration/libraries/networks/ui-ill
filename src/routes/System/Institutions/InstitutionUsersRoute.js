import { useInfiniteQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { generateKiwtQuery, useKiwtSASQuery } from '@k-int/stripes-kint-components';
import { NAMESPACE_ILL } from '@k-int/stripes-ill';
import { useSASQQIndex } from '@folio/stripes-erm-components';

import { InstitutionUsers } from '../../../components/System/Institutions';

import {
  INSTITUTION_USER_ENDPOINT
} from '../../../constants/endpoints';
import { useRequestContext } from '../../../hooks';
import { defaultInstitutionUsersQIndex } from '../../../constants/defaultQIndexes';

const PER_PAGE = 100;

const InstitutionUsersRoute = ({ children }) => {
  const { query, queryGetter, querySetter } = useKiwtSASQuery();
  const ky = useOkapiKy();

  const { requestContext } = useRequestContext();
  const { searchKey } = useSASQQIndex({ defaultQIndex: defaultInstitutionUsersQIndex });


  const SASQ_MAP = {
    searchKey,
    sortKeys: {
    },
    perPage: PER_PAGE
  };

  const institutionUsersQuery = useInfiniteQuery(
    {
      queryKey: [NAMESPACE_ILL, INSTITUTION_USER_ENDPOINT, query, requestContext],
      queryFn: ({ pageParam = 0 }) => ky(`${INSTITUTION_USER_ENDPOINT}${generateKiwtQuery({ offset: pageParam, ...SASQ_MAP }, query)}`).json(),
      useErrorBoundary: true,
    }
  );

  return (
    <InstitutionUsers
      institutionUsersQuery={institutionUsersQuery}
      queryGetter={queryGetter}
      querySetter={querySetter}
    >
      {children}
    </InstitutionUsers>
  );
};

export default InstitutionUsersRoute;
