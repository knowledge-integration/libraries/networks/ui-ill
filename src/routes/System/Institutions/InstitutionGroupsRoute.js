import { useInfiniteQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { generateKiwtQuery, useKiwtSASQuery } from '@k-int/stripes-kint-components';
import { NAMESPACE_ILL } from '@k-int/stripes-ill';
import { useSASQQIndex } from '@folio/stripes-erm-components';

import { InstitutionGroups } from '../../../components/System/Institutions';
import {
  INSTITUTION_GROUP_ENDPOINT
} from '../../../constants/endpoints';
import { useRequestContext } from '../../../hooks';
import { defaultInstitutionUsersQIndex } from '../../../constants/defaultQIndexes';

const PER_PAGE = 100;

const InstitutionGroupsRoute = ({ children }) => {
  const { query, queryGetter, querySetter } = useKiwtSASQuery();
  const ky = useOkapiKy();

  const { searchKey } = useSASQQIndex({ defaultQIndex: defaultInstitutionUsersQIndex });


  const { requestContext } = useRequestContext();

  const SASQ_MAP = {
    searchKey,
    sortKeys: {
    },
    perPage: PER_PAGE
  };

  const institutionGroupQuery = useInfiniteQuery(
    {
      queryKey: [NAMESPACE_ILL, INSTITUTION_GROUP_ENDPOINT, query, requestContext],
      queryFn: ({ pageParam = 0 }) => ky(`${INSTITUTION_GROUP_ENDPOINT}${generateKiwtQuery({ offset: pageParam, ...SASQ_MAP }, query)}`).json(),
      useErrorBoundary: true,
    }
  );

  return (
    <InstitutionGroups
      institutionGroupsQuery={institutionGroupQuery}
      queryGetter={queryGetter}
      querySetter={querySetter}
    >
      {children}
    </InstitutionGroups>
  );
};

export default InstitutionGroupsRoute;
