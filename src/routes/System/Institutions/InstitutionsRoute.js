import { useInfiniteQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { generateKiwtQuery, useKiwtSASQuery } from '@k-int/stripes-kint-components';
import { NAMESPACE_ILL } from '@k-int/stripes-ill';
import { useSASQQIndex } from '@folio/stripes-erm-components';

import { Institutions } from '../../../components/System/Institutions';

import {
  INSTITUTION_ENDPOINT
} from '../../../constants/endpoints';
import { defaultInstitutionsQIndex } from '../../../constants/defaultQIndexes';

import { useRequestContext } from '../../../hooks';

const PER_PAGE = 100;

const InstitutionsRoute = ({ children }) => {
  const { query, queryGetter, querySetter } = useKiwtSASQuery();
  const ky = useOkapiKy();
  const { searchKey } = useSASQQIndex({ defaultQIndex: defaultInstitutionsQIndex });

  const { requestContext } = useRequestContext();

  const SASQ_MAP = {
    searchKey,
    sortKeys: {
    },
    perPage: PER_PAGE
  };

  const institutionQuery = useInfiniteQuery(
    {
      queryKey: [NAMESPACE_ILL, INSTITUTION_ENDPOINT, SASQ_MAP, query, requestContext],
      queryFn: ({ pageParam = 0 }) => ky(`${INSTITUTION_ENDPOINT}${generateKiwtQuery({ offset: pageParam, ...SASQ_MAP }, query)}`).json(),
      useErrorBoundary: true,
    }
  );

  return (
    <Institutions
      institutionsQuery={institutionQuery}
      queryGetter={queryGetter}
      querySetter={querySetter}
    >
      {children}
    </Institutions>
  );
};

export default InstitutionsRoute;
