import {
  useInfiniteQuery
} from 'react-query';

import {
  useOkapiKy
} from '@folio/stripes/core';

import { useSASQQIndex } from '@folio/stripes-erm-components';

import {
  generateKiwtQuery,
  useKiwtSASQuery
} from '@k-int/stripes-kint-components';

import {
  NAMESPACE_ILL
} from '@k-int/stripes-ill';

import SharedIndexQuery from '../../../components/System/SharedIndex/SharedIndexQuery';

import {
  SHARED_INDEX_QUERY_ENDPOINT
} from '../../../constants/endpoints';

const PER_PAGE = 100;

const SharedIndexQueryRoute = ({ children }) => {
  const { query, queryGetter, querySetter } = useKiwtSASQuery();
  const { searchKey } = useSASQQIndex({ defaultQIndex: 'All', defaultQuery: '*' });
  const ky = useOkapiKy();

  const SASQ_MAP = {
    searchKey,
    sortKeys: {
    },
    perPage: PER_PAGE
  };

  const sharedIndexQuery = useInfiniteQuery(
    {
      queryKey: [NAMESPACE_ILL, SHARED_INDEX_QUERY_ENDPOINT, SASQ_MAP, query, '@k-int/sharedIndex'],
      queryFn: ({ pageParam = 0 }) => ky(`${SHARED_INDEX_QUERY_ENDPOINT}${generateKiwtQuery({ offset: pageParam, ...SASQ_MAP }, query)}`).json(),
      useErrorBoundary: true,
      enabled: (query.query ?? '') !== ''
    }
  );

  return (
    <SharedIndexQuery
      sharedIndexQuery={sharedIndexQuery}
      queryGetter={queryGetter}
      querySetter={querySetter}
    >
      {children}
    </SharedIndexQuery>
  );
};

export default SharedIndexQueryRoute;
