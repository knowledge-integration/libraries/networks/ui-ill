import { useLocation } from 'react-router';
import { useMemo } from 'react';
import {
  ADMIN_INSTITUTION_GROUPS, ADMIN_INSTITUTION_USERS,
  ADMIN_INSTITUTIONS,
  BASE_ADMIN_URL,
  BASE_REQUESTS_URL,
  REQUESTS_SHARED_INDEX
} from '../constants/urls';
import { useRequestContext } from './index';

const useILLRouting = () => {
  const { pathname } = useLocation();
  const requestContext = useRequestContext();

  const isRequestTab = useMemo(() => pathname.startsWith(BASE_REQUESTS_URL), [pathname]);
  const isAdminTab = useMemo(() => pathname.startsWith(BASE_ADMIN_URL), [pathname]);

  // Requests groups (first two handled by requestContext hook -- potentially not the best separation)
  const isSharedIndex = useMemo(() => pathname.startsWith(REQUESTS_SHARED_INDEX), [pathname]);

  // Admin groups
  const isInstiutions = useMemo(() => pathname.startsWith(ADMIN_INSTITUTIONS), [pathname]);
  const isInstiutionGroups = useMemo(() => pathname.startsWith(ADMIN_INSTITUTION_GROUPS), [pathname]);
  const isInstiutionUsers = useMemo(() => pathname.startsWith(ADMIN_INSTITUTION_USERS), [pathname]);

  return {
    isAdminTab,
    isRequestTab,
    isSharedIndex,
    ...requestContext, // Supply all of the information needed here as well
    isInstiutions,
    isInstiutionGroups,
    isInstiutionUsers,
  };
};

export default useILLRouting;

