import { useMemo } from 'react';
import { useParams } from 'react-router-dom';

export default () => {
  const { requestContext } = useParams();

  const isBorrower = useMemo(() => requestContext === 'borrower', [requestContext]);
  const isSupplier = useMemo(() => requestContext === 'supplier', [requestContext]);

  return {
    isBorrower,
    isSupplier,
    requestContext
  };
};
