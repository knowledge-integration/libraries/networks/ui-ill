import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withStripes } from '@folio/stripes/core';
import {
  Accordion,
  Card,
  Col,
  KeyValue,
  Row,
} from '@folio/stripes/components';
import { DeveloperJson } from '../../DeveloperJson';

class UserCard extends React.Component {
  static propTypes = {
    user: PropTypes.shape({
      id: PropTypes.string,
      // Maybe other properties
    }),
    stripes: PropTypes.shape({
      config: PropTypes.shape({
        showDevInfo: PropTypes.bool,
      }).isRequired,
    }).isRequired,
  };

  render() {
    const props = { ...this.props };
    // React complains if any of these props are passed in <Card>
    delete props.refreshRemote;
    delete props.dataKey;
    delete props.userId;

    let user = props.user;
    const p = user.personal || {};
    if (user && (p.email || p.lastName || p.firstName)) {
      props.cardStyle = 'positive';
    } else {
      props.cardStyle = 'negative';
      delete props.headerClass;
      delete props.cardClass;
      if (!user) user = {};
    }

    return (
      <Card
        id="requestingUserInfo-card"
        headerStart="User"
        roundedBorder
        {...props}
      >
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.userId" />}
              value={user.id}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.emailAddress" />}
              value={p.email}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.lastName" />}
              value={p.lastName}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.firstName" />}
              value={p.firstName}
            />
          </Col>
        </Row>
        {!props.stripes.config.showDevInfo ? '' :
        <Row>
          <Col xs={12}>
            <Accordion
              id="requestingUserInfo-card-dev"
              label={<FormattedMessage id="ui-ill-ui.information.heading.developer" />}
              closedByDefault
            >
              <DeveloperJson src={user} />
            </Accordion>
          </Col>
        </Row>
        }
      </Card>
    );
  }
}

export default withStripes(UserCard);
