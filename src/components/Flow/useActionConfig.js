import { useSettingSection } from '@k-int/stripes-kint-components';
import { SETTINGS_INSTITUTION_ENDPOINT } from '../../constants/endpoints';

const useActionConfig = () => {
  const { settings } = useSettingSection({
    sectionName: 'state_action_config',
    settingEndpoint: SETTINGS_INSTITUTION_ENDPOINT
  });

  const returnObj = {};

  settings.map(st => returnObj[st.key] = st.value ?? st.defValue);
  return returnObj;
};

export default useActionConfig;
