import React from 'react';
import { Form, Field } from 'react-final-form';
import { FormattedMessage, useIntl } from 'react-intl';
import { Button, Modal, ModalFooter, Select } from '@folio/stripes/components';
import { useIsActionPending, useOkapiQuery } from '@k-int/stripes-ill';
import { required } from '../../../util/validators';
import { CancelModalButton } from '../../ModalButtons';
import { useModal } from '../../MessageModalState';
import {
  PATRON_REQUESTS_MANUAL_CLOSE_STATES_ENDPOINT
} from '../../../constants/endpoints';
import { REQUEST_ACTIONS } from '../../../constants/requestActions';

const Footer = ({ disableSubmit, submit }) => (
  <ModalFooter>
    {/* These appear in the reverse order? */}
    <Button buttonStyle="danger" onClick={submit} disabled={disableSubmit}>
      <FormattedMessage id="ui-ill-ui.actions.manualClose" />
    </Button>
    <CancelModalButton><FormattedMessage id="ui-ill-ui.button.goBack" /></CancelModalButton>
  </ModalFooter>
);

const ManualClose = ({ request, performAction }) => {
  const actionPending = !!useIsActionPending(request.id);
  const [currentModal, setModal] = useModal();
  const closeModal = () => setModal(null);
  const { formatMessage } = useIntl();

  const terminalQuery = useOkapiQuery(PATRON_REQUESTS_MANUAL_CLOSE_STATES_ENDPOINT(request.id), {
    staleTime: 8 * 60 * 1000
  });
  if (!terminalQuery.isSuccess) return null;
  const terminalOptions = terminalQuery.data
    .map(state => ({ value: state.code, label: formatMessage({ id: `stripes-ill.states.${state.code}` }) }));

  const onSubmit = values => {
    return performAction(REQUEST_ACTIONS.manualClose, values, {
      success: 'ui-ill-ui.actions.manualClose.success',
      error: 'ui-ill-ui.actions.manualClose.error',
    })
      .then(closeModal);
  };

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{
        terminalState: terminalOptions[0].value
      }}
      render={({ handleSubmit, submitting, form }) => (
        <form onSubmit={handleSubmit}>
          <Modal
            label={<FormattedMessage id="ui-ill-ui.actions.manualClose" />}
            open={currentModal === 'ManualClose'}
            onClose={closeModal}
            dismissible
            footer={<Footer disableSubmit={submitting || actionPending} submit={form.submit} />}
          >
            <p>
              <FormattedMessage id="ui-ill-ui.actions.manualClose.confirm" values={{ hrid: request.hrid }} />
            </p>
            <p>
              <FormattedMessage id="ui-ill-ui.actions.manualClose.detail" />
            </p>
            <Field
              name="terminalState"
              label={<FormattedMessage id="ui-ill-ui.actions.manualClose.choose" />}
              component={Select}
              dataOptions={terminalOptions}
              required
              validate={required}
            />
          </Modal>
        </form>
      )}
    />
  );
};

export default ManualClose;
