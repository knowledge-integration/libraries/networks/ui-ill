import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Form, Field } from 'react-final-form';
import { Button, Row, Col, TextField } from '@folio/stripes/components';
import { REQUEST_ACTIONS } from '../../../constants/requestActions';

const SupplierFillDigitalLoan = ({ performAction }) => {
  const onSubmit = values => {
    return performAction(REQUEST_ACTIONS.supplierFillDigitalLoan, values, {
      success: 'ui-ill-ui.actions.supplierFillDigitalLoan.success',
      error: 'ui-ill-ui.actions.supplierFillDigitalLoan.error',
    });
  };
  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit, submitting }) => (
        <form onSubmit={handleSubmit} autoComplete="off">
          <FormattedMessage id="ui-ill-ui.actions.supplierFillDigitalLoan.prompt" />
          <Row>
            <Col xs={11}>
              <Field name="url" component={TextField} autoFocus />
            </Col>
            <Col xs={1}>
              <Button buttonStyle="primary mega" type="submit" disabled={submitting}>
                <FormattedMessage id="ui-ill-ui.actions.supplierFillDigitalLoan.button" />
              </Button>
            </Col>
          </Row>
        </form>
      )}
    />
  );
};
SupplierFillDigitalLoan.propTypes = {
  performAction: PropTypes.func.isRequired,
};
export default SupplierFillDigitalLoan;
