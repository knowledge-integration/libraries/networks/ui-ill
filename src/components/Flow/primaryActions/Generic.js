import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import ScanConfirmAction from '../ScanConfirmAction';

const Generic = props => {
  const { name:action, intl } = props;
  const promptKey = `ui-ill-ui.actions.${action}.prompt`;
  const successKey = `stripes-ill.actions.${action}.success`;
  const errorKey = `stripes-ill.actions.${action}.error`;
  if (promptKey in intl.messages && successKey in intl.messages && errorKey in intl.messages) {
    return <ScanConfirmAction
      action={action}
      prompt={promptKey}
      success={successKey}
      error={errorKey}
      {...props}
    />;
  }
  return <ScanConfirmAction
    action={action}
    label={`stripes-ill.actions.${action}`}
    {...props}
  />;
};

Generic.propTypes = {
  name: PropTypes.string.isRequired,
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Generic);
