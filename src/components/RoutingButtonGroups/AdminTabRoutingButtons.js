import { FormattedMessage } from 'react-intl';

import { ResponsiveButtonGroup } from '@k-int/stripes-kint-components';
import { Button } from '@folio/stripes/components';

import { useILLRouting } from '../../hooks';
import {
  ADMIN_INSTITUTION_GROUPS,
  ADMIN_INSTITUTION_USERS,
  ADMIN_INSTITUTIONS
} from '../../constants/urls';

const AdminTabRoutingButtons = () => {
  const { isInstiutions, isInstiutionGroups, isInstiutionUsers } = useILLRouting();

  return (
    <ResponsiveButtonGroup
      fullWidth
      selectedIndex={isInstiutions ? 0 : (isInstiutionGroups ? 1 : 2)} // Bit ugly but oh well
    >
      <Button
        key="admin-tab-institution-button"
        buttonStyle={isInstiutions ? 'primary' : 'default'}
        to={ADMIN_INSTITUTIONS}
      >
        <FormattedMessage id="ui-ill-ui.institutions" defaultMessage="Libraries" />
      </Button>
      <Button
        key="admin-tab-institution-groups-button"
        buttonStyle={isInstiutionGroups ? 'primary' : 'default'}
        to={ADMIN_INSTITUTION_GROUPS}
      >
        <FormattedMessage id="ui-ill-ui.institution.groups" defaultMessage="Groups" />
      </Button>
      <Button
        key="admin-tab-institution-users-button"
        buttonStyle={isInstiutionUsers ? 'primary' : 'default'}
        to={ADMIN_INSTITUTION_USERS}
      >
        <FormattedMessage id="ui-ill-ui.institution.users" defaultMessage="ILL users" />
      </Button>
    </ResponsiveButtonGroup>
  );
};

export default AdminTabRoutingButtons;
