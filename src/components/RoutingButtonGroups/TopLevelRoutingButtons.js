import { FormattedMessage } from 'react-intl';

import { Button, ButtonGroup } from '@folio/stripes/components';
import { BASE_ADMIN_URL, BASE_REQUESTS_URL } from '../../constants/urls';
import { useILLRouting } from '../../hooks';

const TopLevelRoutingButtons = () => {
  const { isAdminTab, isRequestTab } = useILLRouting();

  return (
    <div>
      <ButtonGroup>
        <Button
          key="top-level-requests-button"
          buttonStyle={
            isRequestTab ?
              'primary' :
              'default'
          }
          to={BASE_REQUESTS_URL}
        >
          <FormattedMessage id="ui-ill-ui.patronrequests.requests" />
        </Button>
        <Button
          key="top-level-admin-button"
          buttonStyle={
            isAdminTab ?
              'primary' :
              'default'
          }
          to={BASE_ADMIN_URL}
        >
          <FormattedMessage id="ui-ill-ui.patronrequests.admin" />
        </Button>
      </ButtonGroup>
    </div>
  );
};

export default TopLevelRoutingButtons;
