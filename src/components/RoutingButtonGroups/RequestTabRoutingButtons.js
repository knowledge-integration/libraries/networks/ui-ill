import { FormattedMessage } from 'react-intl';
import { useHistory, useLocation } from 'react-router';

import { ResponsiveButtonGroup } from '@k-int/stripes-kint-components';
import { Button } from '@folio/stripes/components';

import {
  REQUESTS_BORROWER,
  REQUESTS_SHARED_INDEX,
  REQUESTS_SUPPLIER
} from '../../constants/urls';

import { useILLRouting } from '../../hooks';

const RequestTabRoutingButtons = () => {
  const { isBorrower, isSupplier, isSharedIndex } = useILLRouting();
  const { search } = useLocation();
  const history = useHistory();

  // We don't always want to keep search params between screens, only between supplier/borrower
  const getSearchParams = () => {
    if (isBorrower || isSupplier) { return search; }

    return '';
  };

  return (
    <ResponsiveButtonGroup
      fullWidth
      selectedIndex={isBorrower ? 0 : (isSupplier ? 1 : 2)} // Bit ugly but oh well
    >
      <Button
        key="requests-tab-borrow-button"
        buttonStyle={isBorrower ? 'primary' : 'default'}
        onClick={() => history.push(`${REQUESTS_BORROWER}${getSearchParams()}`)}
      >
        <FormattedMessage id="ui-ill-ui.request" defaultMessage="Borrow" />
      </Button>
      <Button
        key="requests-tab-supply-button"
        buttonStyle={isSupplier ? 'primary' : 'default'}
        onClick={() => history.push(`${REQUESTS_SUPPLIER}${getSearchParams()}`)}
      >
        <FormattedMessage id="ui-ill-ui.supply" defaultMessage="Supply" />
      </Button>
      <Button
        key="requests-tab-shared-index-button"
        buttonStyle={isSharedIndex ? 'primary' : 'default'}
        onClick={() => history.push(REQUESTS_SHARED_INDEX)}
      >
        <FormattedMessage id="ui-ill-ui.sharedIndex.query" defaultMessage="Shared index" />
      </Button>
    </ResponsiveButtonGroup>
  );
};

export default RequestTabRoutingButtons;
