import React, { useContext } from 'react';
import { includes, filter } from 'lodash';
import { FormattedMessage } from 'react-intl';
import { Button } from '@folio/stripes/components';
import { CalloutContext, useOkapiKy } from '@folio/stripes/core';
import { useCloseDirect } from '@k-int/stripes-ill';
import AllPullSlips from './PullSlip/AllPullSlips'; // FIXME broken import?
import PrintOrCancel from './PrintOrCancel';
import { PATRON_REQUESTS_ENDPOINT } from '../constants/endpoints';

const PrintAllPullSlips = ({ query: requestsQuery }) => {
  const callout = useContext(CalloutContext);
  const okapiKy = useOkapiKy();
  const close = useCloseDirect();

  const records = requestsQuery?.data?.pages?.flatMap(x => x.results);
  const totalRecords = requestsQuery?.data?.pages?.[0]?.total;

  const markAllPrintableAsPrinted = () => {
    const promises = [];

    requestsQuery?.data?.pages?.flatMap(x => x.results).forEach(record => {
      if (includes(record.validActions, 'supplierPrintPullSlip')) {
        promises.push(okapiKy(`${PATRON_REQUESTS_ENDPOINT}/${record.id}/performAction`, {
          method: 'POST',
          json: { action: 'supplierPrintPullSlip' },
        }).json());
      }
    });

    Promise.all(promises)
      .catch((exception) => {
        callout.sendCallout({ type: 'error', message: `Protocol failure in marking slips as printed: ${exception}` });
      })
      .then((responses) => {
        const failures = filter(responses, r => !r.status);
        if (failures.length === 0) {
          callout.sendCallout({ type: 'success', message: `All slips ${responses.length === 0 ? 'were already ' : ''}marked as printed.` });
        } else {
          const messages = failures.map(f => f.message).join('; ');
          console.error(messages); // eslint-disable-line no-console
          callout.sendCallout({ type: 'error', message: `Some slips not marked as printed: ${messages}` });
        }
        // Need to re-fetch requests to reflect updated states
        requestsQuery.refetch();
      });
  };

  if (!requestsQuery.isSuccess) {
    return 'Record not yet loaded for printing';
  }

  if (records.length < totalRecords) {
    return `Not enough records loaded for printing (${records.length} of ${totalRecords})`;
  }

  return (
    <>
      <PrintOrCancel
        extraButtons={
          <Button
            marginBottom0
            onClick={() => {
              markAllPrintableAsPrinted();
              close();
            }}
          >
            <FormattedMessage id="ui-ill-ui.markAllSlipsPrinted" />
          </Button>
        }
      >
        <AllPullSlips records={records} />
      </PrintOrCancel>
    </>
  );
};

export default PrintAllPullSlips;
