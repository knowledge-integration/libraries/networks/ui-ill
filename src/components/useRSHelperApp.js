import { useCallback } from 'react';

import { FormattedMessage } from 'react-intl';

import { useHelperApp, Tags } from '@k-int/stripes-kint-components';

import { IconButton } from '@folio/stripes/components';
import { ChatPane } from './chat';


const useRSHelperApp = () => {
  const { HelperComponent, helperToggleFunctions, isOpen } = useHelperApp({
    tags: Tags,
    chat: ChatPane
  });

  const TagButton = useCallback(({ request, onClick = () => null }) => (
    <FormattedMessage id="ui-ill-ui.view.showTags">
      {ariaLabel => (
        <IconButton
          icon="tag"
          id="clickable-show-tags"
          badgeCount={request?.tags?.length ?? 0}
          onClick={
            () => {
              helperToggleFunctions.tags();
              onClick({ open: isOpen('tags') });
            }
          }
          ariaLabel={ariaLabel[0]}
        />
      )}
    </FormattedMessage>
  ), [helperToggleFunctions, isOpen]);

  const ChatButton = useCallback(({ request, onClick = () => null }) => {
    const unseenNotifications = request?.notifications?.filter(notification => notification.seen === false && notification.isSender === false)?.length ?? 0;
    return (
      <FormattedMessage id="ui-ill-ui.view.showChat">
        {ariaLabel => (
          <IconButton
            icon="comment"
            id="clickable-show-chat"
            badgeCount={unseenNotifications}
            onClick={
              () => {
                helperToggleFunctions.chat();
                onClick({ open: isOpen('chat') });
              }
            }
            ariaLabel={ariaLabel[0]}
          />
        )}
      </FormattedMessage>
    );
  }, [helperToggleFunctions, isOpen]);

  return { ChatButton, HelperComponent, TagButton, isOpen };
};

export default useRSHelperApp;
