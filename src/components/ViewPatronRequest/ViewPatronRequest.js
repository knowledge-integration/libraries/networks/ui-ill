import React, { useEffect, useRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { useLocation } from 'react-router-dom';
import { useStripes } from '@folio/stripes/core';
import { AccordionSet, Accordion } from '@folio/stripes/components';
import { CatalogInfo } from '@k-int/stripes-ill/cards';
import {
  RequestInfo,
  RequestingInstitutionInfo,
  RequestingUserInfo,
  CitationMetadataInfo,
  SuppliersInfo,
  AuditInfo,
  ProtocolInfo
} from './sections';
import { DeveloperJson } from '../DeveloperJson';
import { useRequestContext } from '../../hooks';

const ViewPatronRequest = ({ record }) => {
  const location = useLocation();
  const stripes = useStripes();

  const { isBorrower, isSupplier } = useRequestContext();

  const scrollToRef = (ref) => {
    return (
      ref.current?.scrollIntoView({ behavior: 'auto', block: 'start' })
    );
  };
  const auditRef = useRef(null);

  useEffect(() => {
    const executeScrollToAuditLog = () => scrollToRef(auditRef);
    if (location?.state?.scrollToAuditTrail) {
      executeScrollToAuditLog();
    }
  }, [location.state]);

  return (
    <AccordionSet>
      {/* No card */}
      <Accordion label={<FormattedMessage id="ui-ill-ui.information.heading.request" />}>
        <RequestInfo id="requestInfo" record={record} />
      </Accordion>
      {/* Blue card */}
      <Accordion id="requestingInstitutionInfo" label={<FormattedMessage id="ui-ill-ui.information.heading.requestinginstitution" closedByDefault />}>
        <RequestingInstitutionInfo id="requestingInstitutionInfo" record={record} />
      </Accordion>
      {/* Gold card */}
      {isBorrower ?
        <Accordion id="requestingUserInfo" label={<FormattedMessage id="ui-ill-ui.information.heading.requester" closedByDefault />}>
          <RequestingUserInfo id="requestingUserInfo" record={record} />
        </Accordion> : null
      }
      {/* Pink card */}
      <Accordion id="citationMetadataInfo" label={<FormattedMessage id="ui-ill-ui.information.heading.citationMetadata" closedByDefault />}>
        <CitationMetadataInfo id="citationMetadataInfo" record={record} />
      </Accordion>
      {/* Pale green card */}
      <Accordion id="catalogInfo" label={<FormattedMessage id="ui-ill-ui.information.heading.catalogInfo" />}>
        <CatalogInfo id="catalogInfo" request={record} />
      </Accordion>
      {/* Green card */}
      {isSupplier ?
        <Accordion id="suppliersInfo" label={<FormattedMessage id="ui-ill-ui.information.heading.suppliers" />}>
          <SuppliersInfo id="suppliersInfo" record={record} />
        </Accordion> : null
      }
      {/* Purple card--div to hold scrolling ref */}
      <div ref={auditRef}>
        <Accordion id="auditInfo" label={<FormattedMessage id="ui-ill-ui.information.heading.audit" />}>
          <AuditInfo id="auditInfo" record={record} />
        </Accordion>
      </div>
      <div>
        <Accordion id="protocolInfo" label={<FormattedMessage id="ui-ill-ui.information.heading.protocol" />}>
          <ProtocolInfo id="protocolInfo" record={record} />
        </Accordion>
      </div>
      {/* No card */}
      {!stripes.config.showDevInfo ? '' :
      <Accordion
        id="developerInfo"
        closedByDefault
        label={<FormattedMessage id="ui-ill-ui.information.heading.developer" />}
        displayWhenClosed={<FormattedMessage id="ui-ill-ui.information.heading.developer.help" />}
      >
        <DeveloperJson src={record} />
      </Accordion>
      }
    </AccordionSet>
  );
};

export default ViewPatronRequest;
