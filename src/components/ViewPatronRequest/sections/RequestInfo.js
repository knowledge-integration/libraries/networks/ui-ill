import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Card,
  Col,
  KeyValue,
  Row,
  FormattedUTCDate,
} from '@folio/stripes/components';
import formattedDateTime from '../../../util/formattedDateTime';

class RequestInfo extends React.Component {
  static propTypes = {
    record: PropTypes.object,
    id: PropTypes.string,
  };

  render() {
    const { record } = this.props;

    return (
      <Card
        id={`${this.props.id}-card`}
        headerStart={record.id}
        roundedBorder
      >
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.hrid" />}
              value={record.hrid}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.fullId" />}
              value={record.id}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.dateSubmitted" />}
              value={formattedDateTime(record.dateCreated)}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.lastUpdated" />}
              value={formattedDateTime(record.lastUpdated)}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.dateNeeded" />}
              value={record.neededBy ? <FormattedUTCDate value={record.neededBy} /> : ''}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.pickupLocation" />}
              value={record.pickupLocation}
            />
          </Col>
        </Row>
        {record?.pickupURL &&
          <Row>
            <Col xs={12}>
              <KeyValue
                label={<FormattedMessage id="ui-ill-ui.information.pickupURL" />}
                value={record.pickupURL}
              />
            </Col>
          </Row>
        }
        {record?.requestIdentifiers?.length > 0 &&
          <Row>
            <Col xs={12}>
              <KeyValue
                label={<FormattedMessage id="ui-ill-ui.information.otherIdentifiers" />}
                value={record.requestIdentifiers.map(ident => `${ident.identifierType}: ${ident.identifier}`).join(', ')}
              />
            </Col>
          </Row>
        }
        <Row>
          <Col xs={12}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.notes" />}
              value={record.patronNote}
            />
          </Col>
        </Row>
      </Card>
    );
  }
}

export default RequestInfo;
