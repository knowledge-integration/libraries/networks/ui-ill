import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Card,
  Col,
  KeyValue,
  Row,
} from '@folio/stripes/components';

import css from './CitationMetadata.css';

class CitationMetadataInfo extends React.Component {
  static propTypes = {
    record: PropTypes.object,
    id: PropTypes.string,
  };

  render() {
    const { record } = this.props;
    let summary = record.title || '[UNKNOWN]';
    let author = record.author;
    const date = record.publicationDate;
    if (date) author = `${author} (${date})`;
    if (record.author) summary = `${author}: ${summary}`;

    const hasISSN = !!record.issn;
    const idKey = `ui-ill-ui.information.${hasISSN ? 'issn' : 'isbn'}`;
    const idValue = record[hasISSN ? 'issn' : 'isbn'];

    return (
      <Card
        id={`${this.props.id}-card`}
        headerStart={summary}
        roundedBorder
        cardClass={css.citationMetadataCard}
        headerClass={css.citationMetadataCardHeader}
      >
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.title" />}
              value={record.title}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.author" />}
              value={record.author}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id={idKey} />}
              value={idValue}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.date" />}
              value={record.publicationDate}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.publisher" />}
              value={record.publisher}
            />
          </Col>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.edition" />}
              value={record.edition}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <KeyValue
              label={<FormattedMessage id="ui-ill-ui.information.volume" />}
              value={record.volume}
            />
          </Col>
        </Row>
      </Card>
    );
  }
}

export default CitationMetadataInfo;
