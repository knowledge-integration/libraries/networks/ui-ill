import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Button } from '@folio/stripes/components';
import { ResponsiveButtonGroup } from '@k-int/stripes-kint-components';
import { useLocation } from 'react-router-dom';

const RequestButtons = ({ isSupplier, isBorrower }) => {
  const location = useLocation();
  const { pathname } = location;
  return (
    <ResponsiveButtonGroup fullWidth>
      <Button
        buttonStyle={isSupplier ? 'primary' : 'default'}
        to="/ill/supplier/requests"
      >
        <FormattedMessage id="ui-ill-ui.supply" defaultMessage="Supply" />
      </Button>
      <Button
        buttonStyle={isBorrower ? 'primary' : 'default'}
        to="/ill/borrower/requests"
      >
        <FormattedMessage id="ui-ill-ui.request" defaultMessage="Borrow" />
      </Button>
      <Button
        buttonStyle={pathname.startsWith('/ill/sharedIndex/sharedIndexQuery') ? 'primary' : 'default'}
        to="/ill/sharedIndex/sharedIndexQuery"
      >
        <FormattedMessage id="ui-ill-ui.sharedIndex" defaultMessage="Shared index" />
      </Button>
    </ResponsiveButtonGroup>
  );
};

export default RequestButtons;
