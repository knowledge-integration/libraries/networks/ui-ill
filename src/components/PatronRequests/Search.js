import { FormattedMessage, useIntl } from 'react-intl';
import { SearchKeyControl } from '@folio/stripes-erm-components';

import { Button, Icon, SearchField } from '@folio/stripes/components';

import { useRequestContext } from '../../hooks';

const Search = ({
  resetAll,
  searchHandlers,
  searchValue,
  searchChanged,
  filterChanged,
  qIndexChanged
}) => {
  const intl = useIntl();

  const { isSupplier } = useRequestContext();

  const searchableIndexes = [
    { key: 'id' },
    { key: 'hrid' },
    { label: 'extid', key: 'requestIdentifiers.identifier' },
    { label: 'requesterGivenName', key: 'patronGivenName' },
    { label: 'requesterSurname', key: 'patronSurname' },
    { key: 'title' },
    { key: 'author' },
    { key: 'issn' },
    { key: 'isbn' },
    { label: 'itemBarcode', key: 'volumes.itemId' },
    { label: 'extidType', key: 'requestIdentifiers.identifierType' },
  ].map(x => ({
    ...x,
    label: intl.formatMessage({ id: `ui-ill-ui.index.${x.label ?? x.key}`, defaultMessage: x.label }),
  }));
  if (isSupplier) searchableIndexes.splice(3, 2);
  return (
    <>
      <SearchField
        autoFocus
        name="query"
        onChange={searchHandlers.query}
        onClear={searchHandlers.reset}
        value={searchValue.query}
      />
      <SearchKeyControl
        options={searchableIndexes}
      />
      <Button
        buttonStyle="primary"
        disabled={!searchValue.query || searchValue.query === ''}
        fullWidth
        type="submit"
      >
        <FormattedMessage id="stripes-smart-components.search" />
      </Button>
      <Button
        buttonStyle="none"
        disabled={!filterChanged && !searchChanged && !qIndexChanged}
        id="clickable-reset-all"
        fullWidth
        onClick={resetAll}
      >
        <Icon icon="times-circle-solid">
          <FormattedMessage id="stripes-smart-components.resetAll" />
        </Icon>
      </Button>
    </>
  );
};

export default Search;
