/* eslint-disable react/jsx-fragments */
import { useContext } from 'react';
import {
  FormattedDate,
  FormattedMessage,
  FormattedTime,
  useIntl
} from 'react-intl';
import { Link, useHistory, useLocation, useRouteMatch } from 'react-router-dom';
import queryString from 'query-string';
import { useQueryClient } from 'react-query';


import { AppIcon, CalloutContext, IfPermission, useOkapiKy } from '@folio/stripes/core';
import { SearchAndSortQuery, PersistedPaneset } from '@folio/stripes/smart-components';
import { useSASQQIndex } from '@folio/stripes-erm-components';

import {
  Badge,
  Button,
  Icon,
  LoadingPane,
  MultiColumnList,
  Pane,
  PaneMenu
} from '@folio/stripes/components';

import {
  BATCH_ENDPOINT,
  PATRON_REQUESTS_GENERATE_PICKLIST_BATCH_ENDPOINT
} from '../../constants/endpoints';

import {
  INSTITUTION_USER_MANAGE_INSTITUTION,
  REQUEST_CREATE,
  SUPPLY_CREATE
} from '../../constants/permissions';

import {
  REQUEST_TAB_FILTER_BUTTON_PANE,
  REQUEST_TAB_PERSISTED_PANESET,
  REQUEST_TAB_RESULTS_PANE
} from '../../constants/paneIds';

import { useRequestContext } from '../../hooks';
import ChangeInstitution from '../System/Institutions/ChangeInstitution';
import Filters from './Filters';
import Search from './Search';
import RequestTabRoutingButtons from '../RoutingButtonGroups/RequestTabRoutingButtons';
import { defaultPatronRequestsQIndex } from '../../constants/defaultQIndexes';
import { BASE_REQUESTS_URL } from '../../constants/urls';

const appDetails = {
  request: {
    title: 'Borrow requests',
    visibleColumns: [
      'flags', 'hrid',
      'dateCreated', 'lastUpdated', 'selectedItemBarcode', 'patronIdentifier', 'state', 'serviceType',
      'supplyingInstitutionSymbol', 'pickupLocation',
      'title',
    ],
    extraFilter: 'r.true',
    intlId: 'supplier',
    institutionFilterId: 'supplier',
    statePrefix: 'REQ',
    createPerm: REQUEST_CREATE
  },
  supply: {
    title: 'Supply requests',
    visibleColumns: [
      'flags', 'hrid',
      'dateCreated', 'lastUpdated', 'state', 'serviceType',
      'requestingInstitutionSymbol', 'selectedItemBarcode', 'pickLocation',
      'pickShelvingLocation', 'title'
    ],
    extraFilter: 'r.false',
    intlId: 'requester',
    institutionFilterId: 'requester',
    statePrefix: 'RES',
    createPerm: SUPPLY_CREATE,
  },
};

const PatronRequests = ({
  requestsQuery: {
    data: {
      pages
    } = {},
    fetchNextPage,
    isFetching,
    isSuccess,
  } = {},
  queryGetter,
  querySetter,
  filterOptions,
  searchParams,
  children
}) => {
  const callout = useContext(CalloutContext);
  const history = useHistory();
  const intl = useIntl();
  const location = useLocation();
  const match = useRouteMatch();
  const okapiKy = useOkapiKy();
  const queryClient = useQueryClient();

  const { qIndexChanged, qIndexSASQProps } = useSASQQIndex({ defaultQIndex: defaultPatronRequestsQIndex });

  const {
    isBorrower,
    requestContext
  } = useRequestContext();

  const requests = pages?.flatMap(x => x.results);
  const totalCount = pages?.[0]?.total;

  // Don't love that this is just hanging out here -- feels like repeatable logic, check how agreements handles sort
  const query = queryGetter() ?? {};
  const sortBase = query.sort ?? '';
  const sortOrder = sortBase.replace(/^-/, '').replace(/,.*/, '');
  const sortDirection = sortOrder.startsWith('-') ? 'descending' : 'ascending';

  const fetchMore = (_askAmount, index) => {
    fetchNextPage({ pageParam: index });
  };

  const onPrintAll = () => {
    okapiKy(`${PATRON_REQUESTS_GENERATE_PICKLIST_BATCH_ENDPOINT}?${searchParams.join('&')}`).then(async res => {
      const { batchId } = await res.json();
      queryClient.invalidateQueries(BATCH_ENDPOINT);
      history.push(`${BASE_REQUESTS_URL}/${requestContext}/requests/batch/${batchId}/pullslip`);
    }).catch(async e => {
      const res = await e?.response?.json();
      const message = intl.formatMessage({ id: 'ui-ill-ui.pullSlip.error' }, { errMsg: res?.error ?? e.message });
      callout.sendCallout({ type: 'error', message });
    });
  };

  const getActionMenu = () => (
    <FormattedMessage id="ui-ill-ui.printAllPullSlips">
      {ariaLabel => (
        <Button
          id="clickable-print-pull-slips"
          aria-label={ariaLabel[0]}
          buttonStyle="dropdownItem"
          onClick={onPrintAll}
        >
          <Icon icon="print"><FormattedMessage id="ui-ill-ui.printPullSlips" /></Icon>
        </Button>
      )}
    </FormattedMessage>
  );

  const { title, visibleColumns, createPerm } = isBorrower ? appDetails.request : appDetails.supply;

  return (
    <>
      <SearchAndSortQuery
        {...qIndexSASQProps}
        initialFilterState={{
          terminal: ['false'],
        }}
        initialSortState={{
          sort: '-dateCreated'
        }}
        queryGetter={queryGetter}
        querySetter={querySetter}
        syncToLocationSearch
      >
        {
        ({
          activeFilters,
          filterChanged,
          getFilterHandlers,
          getSearchHandlers,
          onSort,
          onSubmitSearch,
          resetAll,
          searchChanged,
          searchValue,
        }) => {
          return (
            <PersistedPaneset
              appId="@k-int/ill"
              id={REQUEST_TAB_PERSISTED_PANESET}
            >
              <Pane
                defaultWidth="20%"
                id={REQUEST_TAB_FILTER_BUTTON_PANE} // TODO this will want to be a constant for switching purposes
                paneTitle={<FormattedMessage id="stripes-smart-components.searchAndFilter" />}
              >
                <div>
                  <RequestTabRoutingButtons />
                </div>
                <form onSubmit={onSubmitSearch}>
                  <Search
                    filterChanged={filterChanged}
                    qIndexChanged={qIndexChanged}
                    searchChanged={searchChanged}
                    searchHandlers={getSearchHandlers()}
                    searchValue={searchValue}
                    resetAll={resetAll}
                  />
                  {filterOptions &&
                  <Filters
                    activeFilters={activeFilters.state}
                    filterHandlers={getFilterHandlers()}
                    options={filterOptions}
                    appDetails={appDetails}
                  />
                }
                </form>
              </Pane>
              {isSuccess ?
                <Pane
                  actionMenu={getActionMenu}
                  appIcon={<AppIcon app={requestContext} iconKey="app" size="small" />}
                  defaultWidth="fill"
                  firstMenu={(
                    <PaneMenu>
                      <IfPermission perm={INSTITUTION_USER_MANAGE_INSTITUTION}>
                        <ChangeInstitution />
                      </IfPermission>
                    </PaneMenu>
                )}
                  id={REQUEST_TAB_RESULTS_PANE}
                  lastMenu={(
                    <PaneMenu>
                      {isBorrower ?
                        <IfPermission perm={createPerm}>
                          <Button
                            buttonStyle="primary"
                            id="clickable-new-patron-request"
                            marginBottom0
                            to={`requests/create${location.search}`}
                          >
                            <FormattedMessage id="stripes-smart-components.new" />
                          </Button>
                        </IfPermission>
                        : ''}
                    </PaneMenu>
                )}
                  noOverflow
                  padContent={false}
                  paneSub={isSuccess ?
                    <FormattedMessage
                      id="ui-ill-ui.patronrequests.found"
                      values={{ number: totalCount }}
                    /> : ''}
                  paneTitle={title}
                >
                  <MultiColumnList
                    autosize
                    columnMapping={{
                      flags: '',
                      hrid: <FormattedMessage id="ui-ill-ui.patronrequests.id" />,
                      isRequester: <FormattedMessage id="ui-ill-ui.patronrequests.isRequester" />,
                      dateCreated: <FormattedMessage id="ui-ill-ui.patronrequests.dateCreated" />,
                      lastUpdated: <FormattedMessage id="ui-ill-ui.patronrequests.lastUpdated" />,
                      title: <FormattedMessage id="ui-ill-ui.patronrequests.title" />,
                      patronIdentifier: <FormattedMessage id="ui-ill-ui.patronrequests.patronIdentifier" />,
                      state: <FormattedMessage id="ui-ill-ui.patronrequests.state" />,
                      serviceType: <FormattedMessage id="ui-ill-ui.patronrequests.serviceType" />,
                      requestingInstitutionSymbol: <FormattedMessage id="ui-ill-ui.requester" />,
                      supplyingInstitutionSymbol: <FormattedMessage id="ui-ill-ui.supplier" />,
                      selectedItemBarcode: <FormattedMessage id="ui-ill-ui.patronrequests.selectedItemBarcode" />,
                      pickLocation: <FormattedMessage id="ui-ill-ui.patronrequests.pickLocation" />,
                      pickShelvingLocation: <FormattedMessage id="ui-ill-ui.patronrequests.pickShelvingLocation" />,
                      pickupLocation: <FormattedMessage id="ui-ill-ui.patronrequests.pickupLocation" />,
                    }}
                    columnWidths={{
                      flags: '48px',
                      id: { max: 115 },
                      dateCreated: '96px',
                      state: { min: 84 },
                      serviceType: { max: 80 },
                      selectedItemBarcode: '130px',
                    }}
                    contentData={requests}
                    formatter={{
                      flags: a => {
                        const needsAttention = a?.state?.needsAttention;
                        if (a?.unreadMessageCount > 0) {
                          if (needsAttention) {
                            return (
                              <Badge
                                color="red"
                                aria-label={intl.formatMessage({ id: 'ui-ill-ui.needsAttention' }) + intl.formatMessage({ id: 'ui-ill-ui.unread' })}
                              >
                                {`${a.unreadMessageCount}!`}
                              </Badge>
                            );
                          }
                          return <Badge color="primary" aria-label={intl.formatMessage({ id: 'ui-ill-ui.unread' })}>{a.unreadMessageCount}</Badge>;
                        } else if (needsAttention) return <Badge color="red" aria-label={intl.formatMessage({ id: 'ui-ill-ui.needsAttention' })}>!</Badge>;
                        return '';
                      },
                      isRequester: a => (a.isRequester === true ? '✓' : a.isRequester === false ? '✗' : ''),
                      dateCreated: a => (new Date(a.dateCreated).toLocaleDateString() === new Date().toLocaleDateString()
                        ? <FormattedTime value={a.dateCreated} />
                        : <FormattedDate value={a.dateCreated} />),
                      lastUpdated: a => (new Date(a.lastUpdated).toLocaleDateString() === new Date().toLocaleDateString()
                        ? <FormattedTime value={a.lastUpdated} />
                        : <FormattedDate value={a.lastUpdated} />),
                      patronIdentifier: a => {
                        const { patronGivenName, patronSurname } = a;
                        if (patronGivenName && patronSurname) return `${patronSurname}, ${patronGivenName}`;
                        if (patronSurname) return patronSurname;
                        if (patronGivenName) return patronGivenName;
                        return a.patronIdentifier;
                      },
                      state: a => <FormattedMessage id={`stripes-ill.states.${a.state?.code}`} />,
                      serviceType: a => a.serviceType && a.serviceType.value,
                      supplyingInstitutionSymbol: a => (a?.resolvedSupplier?.owner?.symbolSummary ?? '').replace(/,.*/, ''),
                      pickLocation: a => a.pickLocation && a.pickLocation.name,
                      pickShelvingLocation: a => a.pickShelvingLocation && a.pickShelvingLocation.name,
                      selectedItemBarcode: a => (a.volumes?.length <= 1 ? (a.volumes[0]?.itemId || a.selectedItemBarcode) : <FormattedMessage id="ui-ill-ui.flow.info.itemBarcode.multiVolRequest" />)
                    }}
                    hasMargin
                    isEmptyMessage={
                      <>
                        <FormattedMessage id="ui-ill-ui.patronrequests.notFound" /><br />
                        {location?.search?.includes('filter') &&
                        <Link to={queryString.exclude(`${location.pathname}${location.search}`, ['filters'])}>
                          <FormattedMessage id="ui-ill-ui.patronrequests.withoutFilter" />
                        </Link>
                      }
                      </>
                  }
                    loading={isFetching}
                    onHeaderClick={onSort}
                    onNeedMoreData={fetchMore}
                    onRowClick={(_e, rowData) => history.push(`${match.url}/view/${rowData.id}${location.search}`)}
                    sortOrder={sortOrder}
                    sortDirection={sortDirection}
                    totalCount={totalCount}
                    virtualize
                    visibleColumns={visibleColumns}
                  />
                </Pane>
                : <LoadingPane
                    id={REQUEST_TAB_RESULTS_PANE}
                />
            }
              {children}
            </PersistedPaneset>
          );
        }}
      </SearchAndSortQuery>
    </>
  );
};

export default PatronRequests;
