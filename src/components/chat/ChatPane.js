import React, { useEffect, useRef, useState } from 'react';
import { Form, Field } from 'react-final-form';
import { FormattedMessage } from 'react-intl';

import { Button, Col, Pane, Row, TextArea } from '@folio/stripes/components';
import { useIntlCallout, usePerformAction } from '@k-int/stripes-ill';
import { ChatMessage } from './components';
import css from './ChatPane.css';
import MessageDropdown from './components/MessageDropdown';
import useChatActions from './useChatActions';
import { REQUEST_ACTIONS } from '../../constants/requestActions';

const ENTER_KEY = 13;

const ChatPane = ({
  onToggle,
  request: {
    id: reqId,
    isRequester,
    notifications,
    validActions
  } = {}
}) => {
  const latestMessage = useRef();

  const performAction = usePerformAction(reqId);

  const sendCallout = useIntlCallout();
  const { handleMarkAllRead, handleMessageRead } = useChatActions(reqId);

  const scrollToLatestMessage = () => {
    return latestMessage?.current?.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  const jumpToLatestMessage = () => {
    return latestMessage?.current?.scrollIntoView({ block: 'end' });
  };

  // TODO Maybe no longer need this with hasFired
  /*   const [unreadMessageCount, setUnreadMessageCount] = useState(notifications?.filter(notification => notification.seen === false && notification.isSender === false)?.length ?? 0);

  useEffect(() => {
    setUnreadMessageCount(notifications?.filter(notification => notification.seen === false && notification.isSender === false)?.length ?? 0);
  }, [notifications]); */

  // Ensure this only fires once, on mount
  useEffect(() => {
    /*  if (isOpen('chat') && unreadMessageCount > 0) {
      handleMarkAllRead(true);
    } */
    jumpToLatestMessage();
  /* eslint-disable-next-line react-hooks/exhaustive-deps */
  }, []);

  // Track if new notification has arrived, and if so scroll to it
  const [notificationCount, setNotificationCount] = useState(notifications?.length);
  useEffect(() => {
    if (notificationCount !== notifications?.length) {
      scrollToLatestMessage();
      setNotificationCount(notifications?.length);
    }
  }, [notificationCount, notifications, setNotificationCount]);

  const renderPaneFooter = () => {
    const messageValid = validActions?.includes(REQUEST_ACTIONS.message);
    return (
      <Form
        onSubmit={payload => performAction(REQUEST_ACTIONS.message, (payload || {}), {
          success:'ui-ill-ui.actions.message.success',
          error:'ui-ill-ui.actions.message.error',
          display: 'none',
          noAsync: true
        })}
        render={({ form, handleSubmit, pristine }) => {
          const onEnterPress = async (e) => {
            if (e.keyCode === ENTER_KEY && e.shiftKey === false && !pristine) {
              e.preventDefault();
              if (messageValid) {
                await handleSubmit();
                form.reset();
              } else {
                sendCallout('ui-ill-ui.view.chatPane.stateInvalidMessage', 'error');
                form.reset();
              }
            } else if (e.keyCode === ENTER_KEY && e.shiftKey === false) {
              e.preventDefault();
              form.reset();
            }
          };

          return (
            <form
              id="chatPaneMessageForm"
              onSubmit={async event => {
                await handleSubmit(event);
                form.reset();
              }}
              autoComplete="off"
            >
              <Row>
                <Col xs={1} />
                <Col xs={7}>
                  <Field
                    name="note"
                    component={TextArea}
                    onKeyDown={onEnterPress}
                    autoFocus
                  />
                </Col>
                <Col xs={4}>
                  <Button
                    onClick={async event => {
                      await handleSubmit(event);
                      form.reset();
                    }}
                    disabled={pristine || !messageValid}
                  >
                    <FormattedMessage id="ui-ill-ui.view.chatPane.sendMessage" />
                  </Button>
                </Col>
              </Row>
            </form>
          );
        }}
      />
    );
  };

  const displayMessages = () => {
    if (notifications) {
      // Sort the notifications into order by time recieved/sent
      notifications.sort((a, b) => a.timestamp - b.timestamp);

      return (
        <div className={css.noTopMargin}>
          {notifications.map((notification, index) => (
            <ChatMessage
              key={`notificationMessage[${index}]`}
              notification={notification}
              ref={index === notifications.length - 1 ? latestMessage : null}
              handleMessageRead={handleMessageRead}
            />
          ))}
        </div>
      );
    }
    return null;
  };

  return (
    <Pane
      defaultWidth="30%"
      dismissible
      footer={renderPaneFooter()}
      id="request-flow-chat"
      lastMenu={
        <MessageDropdown
          actionItems={[
            {
              label: <FormattedMessage id="ui-ill-ui.view.chatPane.actions.markAllAsRead" />,
              onClick: () => handleMarkAllRead(true)
            },
            {
              label: <FormattedMessage id="ui-ill-ui.view.chatPane.actions.markAllAsUnread" />,
              onClick: () => handleMarkAllRead(false)
            }
          ]}
        />
      }
      onClose={onToggle}
      paneTitle={<FormattedMessage id="ui-ill-ui.view.chatPane" values={{ chatOtherParty: isRequester ? 'supplier' : 'requester' }} />}
    >
      {displayMessages()}
    </Pane>
  );
};

export default ChatPane;
