import { usePerformAction } from '@k-int/stripes-ill';
import { REQUEST_ACTIONS } from '../../constants/requestActions';

const useChatActions = (reqId) => {
  const performAction = usePerformAction(reqId);

  const handleMarkAllRead = (readStatus, excluding = false) => {
    const success = readStatus ? 'ui-ill-ui.actions.messagesAllSeen.success' : 'ui-ill-ui.actions.messagesAllUnseen.success';
    const error = readStatus ? 'ui-ill-ui.actions.messagesAllSeen.error' : 'ui-ill-ui.actions.messagesAllUnseen.error';
    performAction(REQUEST_ACTIONS.messagesAllSeen,
      { seenStatus: readStatus, excludes: excluding },
      { success, error, display: 'none', noAsync: true });
  };

  const handleMessageRead = (notification, currentReadStatus) => {
    const id = notification?.id;

    const success = currentReadStatus ? 'ui-ill-ui.actions.messageSeen.success' : 'ui-ill-ui.actions.messageUnseen.success';
    const error = currentReadStatus ? 'ui-ill-ui.actions.messageSeen.error' : 'ui-ill-ui.actions.messageUnseen.error';

    const payload = { id, seenStatus: false };
    if (!currentReadStatus) {
      payload.seenStatus = true;
    }
    performAction(REQUEST_ACTIONS.messageSeen, payload, { success, error, display: 'none', noAsync: true });
  };

  return { handleMarkAllRead, handleMessageRead };
};

export default useChatActions;
