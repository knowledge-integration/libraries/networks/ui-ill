/* eslint-disable import/prefer-default-export */
import {
  FormattedMessage
} from 'react-intl';

import {
  INSTITUTION_USER_CREATE,
  INSTITUTION_USER_DELETE,
  INSTITUTION_USER_EDIT
} from '../../../constants/permissions';

import {
  INSTITUTION_USER_ENDPOINT,
  INSTITUTION_USER_CREATE_EDIT_DETAILS_ENDPOINT
} from '../../../constants/endpoints';

import {
  SYSTEM_AREA_INSTITUTION,
  SYSTEM_AREA_INSTITUTION_GROUP,
  SYSTEM_AREA_SHARED_INDEX_QUERY,
} from '../SystemAreas';

import SystemSearchAndSort from '../SystemSearchAndSort';
import InstitutionUser from './InstitutionUser';
import InstitutionUserForm from './InstitutionUserForm';
import {
  ADMIN_TAB_FILTER_BUTTON_PANE,
  ADMIN_TAB_PERSISTED_PANESET,
  ADMIN_TAB_RESULTS_PANE
} from '../../../constants/paneIds';
import { AdminTabRoutingButtons } from '../../RoutingButtonGroups';

const InstitutionUsers = ({
  institutionUsersQuery,
  queryGetter,
  querySetter,
  children
}) => {
  const onIninitialiseValues = (record) => {
    let initialValues;
    if (record === null) {
      initialValues = {
        tGroups: []
      };
    } else {
      initialValues = {
        ...record,
        tGroups: record.institutionGroups.map((group) => {
          return ({ label: group.name, value: group.id });
        })
      };
    }
    return (initialValues);
  };

  const onBeforeSave = (record) => {
    // Setup the groups correctly
    record.institutionGroups = record.tGroups.map((group) => {
      return ({ id: group.value });
    });
  };

  return (
    <SystemSearchAndSort
      area="institutionUser"
      filterPaneId={ADMIN_TAB_FILTER_BUTTON_PANE}
      FilterPaneTopRender={AdminTabRoutingButtons}
      panesetId={ADMIN_TAB_PERSISTED_PANESET}
      results={ADMIN_TAB_RESULTS_PANE}
      messageIdPrefix="institution.user"
      detailsTitleMessageId="institution.user"
      visibleColumns={[
        'name',
        'folioUserId'
      ]}
      columnMappings={{
        name: <FormattedMessage id="ui-ill-ui.institution.users.name" />,
        folioUserId: <FormattedMessage id="ui-ill-ui.institution.users.folioUserId" />
      }}
      formatter={{
      }}
      idField="id"
      nameField="name"
      createPerm={INSTITUTION_USER_CREATE}
      deletePerm={INSTITUTION_USER_DELETE}
      editPerm={INSTITUTION_USER_EDIT}
      links={[
        SYSTEM_AREA_INSTITUTION,
        SYSTEM_AREA_INSTITUTION_GROUP,
        SYSTEM_AREA_SHARED_INDEX_QUERY,
      ]}
      query={institutionUsersQuery}
      queryGetter={queryGetter}
      querySetter={querySetter}
      ViewPane={InstitutionUser}
      areaCreateEditDetailsEndpoint={INSTITUTION_USER_CREATE_EDIT_DETAILS_ENDPOINT}
      areaRecordEndpoint={`${INSTITUTION_USER_ENDPOINT}/`}
      areaEndpoint={INSTITUTION_USER_ENDPOINT}
      FormComponent={InstitutionUserForm}
      onInitialiseValues={onIninitialiseValues}
      onBeforeSave={onBeforeSave}
      {...children}
    />
  );
};

export { InstitutionUsers };
