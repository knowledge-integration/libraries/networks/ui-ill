/* eslint-disable react/jsx-fragments */
import {
  Fragment
} from 'react';

import {
  FormattedMessage
} from 'react-intl';

import {
  Row,
  Col,
  KeyValue
} from '@folio/stripes/components';

import {
  useOkapiQuery
} from '@k-int/stripes-ill';

import {
  INSTITUTION_ENDPOINT
} from '../../../constants/endpoints';

import GroupInfo from './sections/GroupInfo';
import UserInfo from './sections/UserInfo';

const Institution = ({ id }) => {
  const institutionQuery = useOkapiQuery(`${INSTITUTION_ENDPOINT}/${id}`);

  // If query not a success just return
  if (!institutionQuery.isSuccess) {
    return null;
  }
  const record = institutionQuery.data;

  return (
    <Fragment>
      <Row>
        <Col xs={12}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-ui.institution.name" />}
            value={record.name}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-ui.institution.description" />}
            value={record.description}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <KeyValue
            label={<FormattedMessage id="ui-ill-ui.institution.directoryEntry" />}
            value={record.directoryEntry?.name}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <GroupInfo
            groups={record.institutionGroups}
            parentId={record.id}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <UserInfo
            parentId={record.id}
          />
        </Col>
      </Row>
    </Fragment>
  );
};

export default Institution;
