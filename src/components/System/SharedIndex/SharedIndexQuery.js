import {
  FormattedMessage
} from 'react-intl';

import SharedIndexRecord from './SharedIndexRecord';
import SystemSearchAndSort from '../SystemSearchAndSort';

import {
  SYSTEM_AREA_INSTITUTION,
  SYSTEM_AREA_INSTITUTION_GROUP,
  SYSTEM_AREA_INSTITUTION_USER,
} from '../SystemAreas';

import {
  REQUEST_TAB_FILTER_BUTTON_PANE,
  REQUEST_TAB_PERSISTED_PANESET,
  REQUEST_TAB_RESULTS_PANE
} from '../../../constants/paneIds';
import { RequestTabRoutingButtons } from '../../RoutingButtonGroups';

const SharedIndexQuery = ({ sharedIndexQuery, queryGetter, querySetter, children }) => {
  const getActionMenu = () => (
    ''
  );

  return (
    <SystemSearchAndSort
      actionsMenu={getActionMenu}
      area="sharedIndex"
      columnMappings={{
        bibClusterId: <FormattedMessage id="ui-ill-ui.sharedIndex.query.bibClusterId" />,
        title: <FormattedMessage id="ui-ill-ui.sharedIndex.query.title" />
      }}
      detailsTitleMessageId="sharedIndex.record"
      filterPaneId={REQUEST_TAB_FILTER_BUTTON_PANE}
      FilterPaneTopRender={RequestTabRoutingButtons}
      formatter={{
      }}
      idField="bibClusterId"
      links={[
        SYSTEM_AREA_INSTITUTION,
        SYSTEM_AREA_INSTITUTION_GROUP,
        SYSTEM_AREA_INSTITUTION_USER,
      ]}
      nameField="title"
      messageIdPrefix="sharedIndex.query"
      panesetId={REQUEST_TAB_PERSISTED_PANESET}
      query={sharedIndexQuery}
      queryGetter={queryGetter}
      querySetter={querySetter}
      resultsPaneId={REQUEST_TAB_RESULTS_PANE}
      source={{ // Fake source from useQuery return values -- this is because no search in sharedIndex results in an empty results pane;
        totalCount: () => sharedIndexQuery.data.totalCount,
        loaded: () => sharedIndexQuery.isFetched,
        pending: () => sharedIndexQuery.isLoading,
        failure: () => sharedIndexQuery.isError,
        failureMessage: () => sharedIndexQuery.error.message
      }}
      ViewPane={SharedIndexRecord}
      visibleColumns={[
        'bibClusterId',
        'title'
      ]}
      {...children}
    />
  );
};

export default SharedIndexQuery;
