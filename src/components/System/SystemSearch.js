import {
  FormattedMessage,
  useIntl
} from 'react-intl';

import {
  Button,
  Icon,
  SearchField
} from '@folio/stripes/components';
import { SearchKeyControl } from '@folio/stripes-erm-components';

const SystemSearch = ({
  filterChanged,
  qIndexChanged,
  resetAll,
  searchChanged,
  searchHandlers,
  searchValue,
  searchableIndexes
}) => {
  const intl = useIntl();
  const searchableIndexesUpdated = searchableIndexes?.map(x => ({
    ...x,
    label: intl.formatMessage({ id: `ui-ill-ui.systemSearchField.${x.label ?? x.key}`, defaultMessage: x.label }),
  }));

  return (
    <>
      <SearchField
        autoFocus
        name="query"
        onChange={searchHandlers.query}
        onClear={searchHandlers.reset}
        value={searchValue.query}
      />
      {searchableIndexesUpdated ?
        <SearchKeyControl
          options={searchableIndexesUpdated}
        /> :
        null}
      <Button
        buttonStyle="primary"
        disabled={!searchValue.query || searchValue.query === ''}
        fullWidth
        type="submit"
      >
        <FormattedMessage id="stripes-smart-components.search" />
      </Button>
      <Button
        buttonStyle="none"
        disabled={!filterChanged && !searchChanged && !qIndexChanged}
        id="clickable-reset-all"
        fullWidth
        onClick={resetAll}
      >
        <Icon icon="times-circle-solid">
          <FormattedMessage id="stripes-smart-components.resetAll" />
        </Icon>
      </Button>
    </>
  );
};

export default SystemSearch;
