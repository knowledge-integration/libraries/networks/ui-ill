/* eslint-disable react/jsx-fragments */
import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useQueryClient } from 'react-query';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';

import {
  PersistedPaneset,
  SearchAndSortNoResultsMessage,
  SearchAndSortQuery
} from '@folio/stripes/smart-components';
import {
  Button,
  LoadingPane,
  MultiColumnList,
  Pane,
  PaneMenu,
} from '@folio/stripes/components';
import { AppIcon, IfPermission } from '@folio/stripes/core';
import { useSASQQIndex } from '@folio/stripes-erm-components';

import { NAMESPACE_ILL } from '@k-int/stripes-ill';

import {
  STATUS_DETAILS_VIEW_CLOSED,
  STATUS_DETAILS_VIEW_CREATE,
  STATUS_DETAILS_VIEW_EDIT,
  STATUS_DETAILS_VIEW_OPEN
} from './DetailsViewConstants';
import DetailsView from './DetailsView';
import DetailsViewCreateEdit from './DetailsViewCreateEdit';
import SystemSearch from './SystemSearch';

const SystemSearchAndSort = ({
  // (similar to SASQRoute which we may eventually migrate to)
  // Allow passing in of an arbitrary filter pane component above search
  FilterPaneTopRender = () => null,
  FormComponent,
  ViewPane,
  actionsMenu,
  area,
  areaCreateEditDetailsEndpoint,
  areaEndpoint,
  areaRecordEndpoint,
  children,
  columnMappings,
  createPerm,
  deletePerm,
  detailsTitleMessageId,
  editPerm,
  filterPaneId,
  formatter,
  idField,
  messageIdPrefix,
  nameField,
  onBeforeSave,
  onInitialiseValues,
  panesetId,
  query,
  queryGetter,
  querySetter,
  resultsPaneId,
  sasqIndexProps = {},
  searchableIndexes,
  source,
  visibleColumns,
}) => {
  const location = useLocation();
  const queryClient = useQueryClient();
  const [selectedRecord, setSelectedRecord] = useState(null);
  const [detailsViewState, setDetailsViewState] = useState(STATUS_DETAILS_VIEW_CLOSED);

  const { qIndex, qIndexChanged, qIndexSASQProps } = useSASQQIndex(sasqIndexProps);
  // TODO for now we have a special case where there is no passed defaultQIndex and qIndex is undefined
  // THIS SHOULD BE REFLECTED IN QINDEXCHANGED IN ERM-COMPS PROBABLY
  const realQIndexChanged = qIndexChanged && !!sasqIndexProps.defaultQIndex && !!qIndex;

  const data = query?.data?.pages?.flatMap(x => x.results);
  const totalCount = query?.data?.pages?.[0]?.total;
  const parsedParams = queryString.parse(location.search);
  const sortOrder = parsedParams.sort || '';
  const fetchMore = (_askAmount, index) => {
    query.fetchNextPage({ pageParam: index });
  };

  const setViewStatus = (status) => {
    setDetailsViewState(status);
  };

  const setRecord = (record) => {
    setSelectedRecord(record);
  };

  const postSaveUpdate = (resetQuery) => {
    // Do we reset the query cache
    if (resetQuery || (selectedRecord !== null)) {
      // A Create, Delete or Edit operation has occureed so invalidate the cache
      queryClient.invalidateQueries({ queryKey: [NAMESPACE_ILL, areaEndpoint], exact: false });
    }
  };

  const isSelected = (item) => {
    // Is it selected
    return ((selectedRecord !== null) && (item.item[idField] === selectedRecord[idField]));
  };

  return (
    <>
      <SearchAndSortQuery
        {...qIndexSASQProps}
        initialSortState={{
          sort: 'name'
        }}
        key={location.search}
        queryGetter={queryGetter}
        querySetter={querySetter}
        syncToLocationSearch
      >
        {
        ({
          filterChanged,
          getSearchHandlers,
          onSort,
          onSubmitSearch,
          resetAll,
          searchChanged,
          searchValue,
        }) => (
          <div>
            <PersistedPaneset
              appId="@k-int/ill"
              id={panesetId ?? `paneset-${area}`}
            >
              <Pane
                defaultWidth="20%"
                id={filterPaneId}
                paneTitle={<FormattedMessage id="stripes-smart-components.searchAndFilter" />}
              >
                <FilterPaneTopRender />
                <form onSubmit={onSubmitSearch}>
                  <SystemSearch
                    filterChanged={filterChanged}
                    qIndexChanged={realQIndexChanged}
                    searchChanged={searchChanged}
                    searchableIndexes={searchableIndexes}
                    searchHandlers={getSearchHandlers()}
                    searchValue={searchValue}
                    resetAll={resetAll}
                  />
                </form>
              </Pane>
              {!query.isLoading ?
                <Pane
                  actionMenu={actionsMenu}
                  appIcon={<AppIcon app="Requests" iconKey="app" size="small" />}
                  defaultWidth="fill"
                  id={resultsPaneId ?? 'system-sasq-results-pane'}
                  lastMenu={(
                    <PaneMenu>
                      {createPerm &&
                        <IfPermission perm={createPerm}>
                          <Button
                            buttonStyle="primary"
                            id={`clickable-ill-${area}.create`}
                            marginBottom0
                            onClick={() => {
                              setRecord(null);
                              setViewStatus(STATUS_DETAILS_VIEW_CREATE);
                            }}
                          >
                            <FormattedMessage id="ui-ill-ui.create" />
                          </Button>
                        </IfPermission>
                      }
                    </PaneMenu>
                  )}
                  noOverflow
                  padContent={false}
                  paneSub={query?.isSuccess ?
                    <FormattedMessage
                      id={`ui-ill-ui.${messageIdPrefix}s.found`}
                      values={{ number: totalCount }}
                    /> : ''}
                  paneTitle={<FormattedMessage id={`ui-ill-ui.${messageIdPrefix}s`} />}
                >
                  <MultiColumnList
                    // onRowClick={(_e, record) => history.push(`./${area}/${record.id}`)}
                    onRowClick={(_e, record) => {
                      setRecord(record);
                      setDetailsViewState(STATUS_DETAILS_VIEW_OPEN);
                    }}
                    showSingleResult
                    autosize
                    columnMapping={columnMappings}
                    columnWidths={{
                    }}
                    contentData={data}
                    formatter={formatter}
                    hasMargin
                    isEmptyMessage={
                      source ? (
                        <SearchAndSortNoResultsMessage
                          filterPaneIsVisible
                          searchTerm={query.query ?? ''}
                          source={source}
                        />
                      ) : <><FormattedMessage id={`ui-ill-ui.${messageIdPrefix}s.notFound`} /><br /></>
                    }
                    isSelected={isSelected}
                    loading={query?.isFetching}
                    onHeaderClick={onSort}
                    onNeedMoreData={fetchMore}
                    sortOrder={sortOrder.replace(/^-/, '').replace(/,.*/, '')}
                    sortDirection={sortOrder.startsWith('-') ? 'descending' : 'ascending'}
                    totalCount={totalCount}
                    virtualize
                    visibleColumns={visibleColumns}
                  />
                </Pane>
                : <LoadingPane
                    id={resultsPaneId ?? 'system-sasq-results-pane'}
                />
              }
              {((detailsViewState === STATUS_DETAILS_VIEW_CLOSED) ||
                ((selectedRecord === null) &&
                (detailsViewState !== STATUS_DETAILS_VIEW_CREATE))) ?
                '' :
                ((detailsViewState === STATUS_DETAILS_VIEW_CREATE) ||
                  (detailsViewState === STATUS_DETAILS_VIEW_EDIT)) ? (
                    <DetailsViewCreateEdit
                      messageIdPrefix={messageIdPrefix}
                      recordIdentifier={selectedRecord?.id}
                      areaCreateEditDetailsEndpoint={areaCreateEditDetailsEndpoint}
                      areaRecordEndpoint={areaRecordEndpoint}
                      areaEndpoint={areaEndpoint}
                      FormComponent={FormComponent}
                      onInitialiseValues={onInitialiseValues}
                      onBeforeSave={onBeforeSave}
                      postSaveUpdate={postSaveUpdate}
                      setViewStatus={setViewStatus}
                    />
                  ) : (
                    <DetailsView
                      record={selectedRecord}
                      area={area}
                      messageIdPrefix={messageIdPrefix}
                      detailsTitleMessageId={detailsTitleMessageId}
                      nameField={nameField}
                      setDetailsViewStatus={setViewStatus}
                      ViewPane={ViewPane}
                      deletePerm={deletePerm}
                      editPerm={editPerm}
                      areaRecordEndpoint={areaRecordEndpoint}
                      postDelete={postSaveUpdate}
                    />
                  )
              }
              {children}
            </PersistedPaneset>
          </div>
        )
      }
      </SearchAndSortQuery>
    </>
  );
};

export default SystemSearchAndSort;
