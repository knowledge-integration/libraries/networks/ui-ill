export const REQUEST_TAB_PERSISTED_PANESET = 'requests-tab-paneset';
export const REQUEST_TAB_FILTER_BUTTON_PANE = 'requests-tabs-filter-pane';
export const REQUEST_TAB_RESULTS_PANE = 'requests-tabs-results-pane';

export const ADMIN_TAB_PERSISTED_PANESET = 'admin-tab-paneset';
export const ADMIN_TAB_FILTER_BUTTON_PANE = 'admin-tabs-filter-pane';
export const ADMIN_TAB_RESULTS_PANE = 'admin-tabs-results-pane';
