// TODO I find this really difficult to read, refactor some of the base names and organise this lot
// I think ENDPOINT_SEPARATOR is largely useless... endpoints are endpoints,
// we're not going to change the routing wholesale. However it's used elsewhere (directory)

export const BASE_ILL_ENDPOINT = 'ill';

export const BATCH_ENDPOINT = `${BASE_ILL_ENDPOINT}/batch`;
export const CUSTOM_PROPERTIES_ENDPOINT = `${BASE_ILL_ENDPOINT}/custprops`;

export const DIRECTORY_ENDPOINT = `${BASE_ILL_ENDPOINT}/directory`;

export const DIRECTORY_ENTRIES_ENDPOINT = `${DIRECTORY_ENDPOINT}/entry`;

export const DIRECTORY_ENTRIES_VALIDATE_ENDPOINT = `${DIRECTORY_ENTRIES_ENDPOINT}/validate`;
export const DIRECTORY_NAMING_AUTHORITY_ENDPOINT = `${DIRECTORY_ENDPOINT}/namingAuthority`;
export const DIRECTORY_SERVICE_ENDPOINT = `${DIRECTORY_ENDPOINT}/service`;

export const FILE_DEFINITION_ENDPOINT = `${BASE_ILL_ENDPOINT}/fileDefinition`;
export const FILE_DEFINITION_FILE_UPLOAD_ENDPOINT = `${FILE_DEFINITION_ENDPOINT}/fileUpload`;

export const HOST_LMS_LOAN_POLICIES_ENDPOINT = `${BASE_ILL_ENDPOINT}/hostLMSItemLoanPolicy`;

export const HOST_LMS_LOCATIONS_ENDPOINT = `${BASE_ILL_ENDPOINT}/hostLMSLocations`;

export const HOST_LMS_PATRON_PROFILES_ENDPOINT = `${BASE_ILL_ENDPOINT}/hostLMSPatronProfiles`;

export const INSTITUTION_ENDPOINT = `${BASE_ILL_ENDPOINT}/institution`;
export const INSTITUTION_CREATE_EDIT_DETAILS_ENDPOINT = `${INSTITUTION_ENDPOINT}/createEditDetails`;

export const INSTITUTION_GROUP_ENDPOINT = `${BASE_ILL_ENDPOINT}/institutionGroup`;
export const INSTITUTION_GROUP_CREATE_EDIT_DETAILS_ENDPOINT = `${INSTITUTION_GROUP_ENDPOINT}/createEditDetails`;

export const INSTITUTION_USER_ENDPOINT = `${BASE_ILL_ENDPOINT}/institutionUser`;
export const INSTITUTION_USER_CREATE_EDIT_DETAILS_ENDPOINT = `${INSTITUTION_USER_ENDPOINT}/createEditDetails`;
export const INSTITUTION_USER_CAN_MANAGE_ENDPOINT = `${INSTITUTION_USER_ENDPOINT}/canManage`;
export const INSTITUTION_USER_MANAGE_INSTITUTION_ENDPOINT = `${INSTITUTION_USER_ENDPOINT}/manageInstitution`;

export const NOTICE_POLICIES_ENDPOINT = `${BASE_ILL_ENDPOINT}/noticePolicies`;

export const REFDATA_ENDPOINT = `${BASE_ILL_ENDPOINT}/refdata`;

export const REPORT_ENDPOINT = `${BASE_ILL_ENDPOINT}/report`;
export const REPORT_WITH_SEPARATOR_ENDPOINT = `${REPORT_ENDPOINT}/`;
export const REPORT_CREATE_UPDATE_ENDPOINT = `${REPORT_ENDPOINT}/createUpdate`;
export const REPORT_GENERATE_PICKLIST_ENDPOINT = `${REPORT_ENDPOINT}/generatePicklist`;

export const SETTINGS_BASE_ENDPOINT = `${BASE_ILL_ENDPOINT}/settings`;
export const SETTINGS_ENDPOINT = `${SETTINGS_BASE_ENDPOINT}/appSettings`;
export const SETTINGS_INSTITUTION_ENDPOINT = `${SETTINGS_BASE_ENDPOINT}/institutionSetting`;
export const SETTINGS_SYSTEM_ENDPOINT = `${SETTINGS_BASE_ENDPOINT}/systemSetting`;

export const SHARED_INDEX_ENDPOINT = `${BASE_ILL_ENDPOINT}/sharedIndexQuery`;
export const SHARED_INDEX_AVAILABILITY_ENDPOINT = `${SHARED_INDEX_ENDPOINT}/availability`;
export const SHARED_INDEX_FIND_MORE_SUPPLIERS_ENDPOINT = `${SHARED_INDEX_ENDPOINT}/findMoreSuppliers`;
export const SHARED_INDEX_QUERY_ENDPOINT = `${SHARED_INDEX_ENDPOINT}/byQuery`;

export const SHELVING_LOCATIONS_ENDPOINT = `${BASE_ILL_ENDPOINT}/shelvingLocations`;

export const SHELVING_LOCATION_SITES_ENDPOINT = `${BASE_ILL_ENDPOINT}/shelvingLocationSites`;

export const TEMPLATES_ENDPOINT = `${BASE_ILL_ENDPOINT}/template`;
export const TAGS_ENDPOINT = `${BASE_ILL_ENDPOINT}/tags`;
export const TIMERS_ENDPOINT = `${BASE_ILL_ENDPOINT}/timers`;

export const PATRON_REQUESTS_ENDPOINT = `${BASE_ILL_ENDPOINT}/patronrequests`;
export const PATRON_REQUESTS_GENERATE_PICKLIST_BATCH_ENDPOINT = `${PATRON_REQUESTS_ENDPOINT}/generatePickListBatch`;
export const PATRON_REQUESTS_MARK_BATCH_AS_PRINTED_ENDPOINT = `${PATRON_REQUESTS_ENDPOINT}/markBatchAsPrinted`;
export const PATRON_REQUESTS_NEW_REQUEST_DETAILS_ENDPOINT = `${PATRON_REQUESTS_ENDPOINT}/newRequestDetails`;

// TODO We can almost certainly make some of the above endpoints into util functions too
export const PATRON_REQUESTS_MANUAL_CLOSE_STATES_ENDPOINT = (id) => `${PATRON_REQUESTS_ENDPOINT}/${id}/manualCloseStates`;
