export const BASE_DIRECTORY_ENTRIES_URL = '/ill-directory/entries';

// View record
export const DIRECTORY_ENTRIES_VIEW_URL_BASE = `${BASE_DIRECTORY_ENTRIES_URL}/view`;
export const DIRECTORY_ENTRIES_VIEW_URL = (id) => `${DIRECTORY_ENTRIES_VIEW_URL_BASE}/${id}`;


// ILL APP

// Two base paths, "requests" vs "admin"
export const BASE_REQUESTS_URL = '/ill/requests';
export const REQUESTS_SHARED_INDEX = `${BASE_REQUESTS_URL}/sharedIndexQuery`;
export const REQUESTS_BORROWER = `${BASE_REQUESTS_URL}/borrower`;
export const REQUESTS_SUPPLIER = `${BASE_REQUESTS_URL}/supplier`;

export const REQUESTS_WITH_CONTEXT = `${BASE_REQUESTS_URL}/:requestContext`;
export const REQUESTS_CONTEXT_REQUESTS = `${REQUESTS_WITH_CONTEXT}/requests`;
export const REQUESTS_CONTEXT_REQUESTS_VIEW = `${REQUESTS_CONTEXT_REQUESTS}/view/:id`;

export const BASE_ADMIN_URL = '/ill/admin';
export const ADMIN_INSTITUTIONS = `${BASE_ADMIN_URL}/institutions`;
export const ADMIN_INSTITUTION_GROUPS = `${BASE_ADMIN_URL}/institutionGroups`;
export const ADMIN_INSTITUTION_USERS = `${BASE_ADMIN_URL}/institutionUsers`;

