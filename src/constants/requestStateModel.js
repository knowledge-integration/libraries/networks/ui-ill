/* eslint-disable import/prefer-default-export */
export const REQUEST_STATE_MODEL = {
  requesterDigitalReturnable: 'DigitalReturnableRequester',
  responderCDL: 'CDLResponder'
};
