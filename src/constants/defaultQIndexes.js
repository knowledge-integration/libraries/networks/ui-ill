export const defaultPatronRequestsQIndex = 'id,hrid,patronGivenName,patronSurname,title,author,issn,isbn,volumes.itemId';

export const defaultInstitutionsQIndex = 'name,description';
export const defaultInstitutionGroupsQIndex = 'name,description';
export const defaultInstitutionUsersQIndex = 'name';
