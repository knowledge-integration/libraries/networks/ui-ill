## [1.13.5](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/compare/v1.13.4...v1.13.5) (2023-11-02)


### Bug Fixes

* **semantic-commit:** Add a semantic commit in lieu of the previous fix commit to bump the version ([eb978df](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/eb978df15d6ced8897143d0a623f3280bb2fc713))

## [1.13.4](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/compare/v1.13.3...v1.13.4) (2023-10-21)


### Bug Fixes

* **build:** Align command for producing module descriptor ([e7f8a1b](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/e7f8a1bc47a1bc5d65d4c1904892e11f0a3d0b1d))

## [1.13.3](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/compare/v1.13.2...v1.13.3) (2023-10-21)


### Bug Fixes

* **build:** Updated CI script to publish Module Descriptor ([4e29005](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/4e29005d39f4984ca8a1b10c3b057e76812cd4ce))

## [1.13.2](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/compare/v1.13.1...v1.13.2) (2023-10-04)


### Bug Fixes

* **build:** add public publishconfig ([7e9dda0](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/7e9dda05ff51ff6705934076ffac3005ef805f10))

## [1.13.1](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/compare/v1.13.0...v1.13.1) (2023-10-04)


### Bug Fixes

* **build:** Attempting to align versions ([2091437](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/20914372824157f2b74a3344c6037d2457288032))
* **build:** Attempting to align versions ([f2b15a5](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/f2b15a5cf0ae1e3d7d14ce1c07bb615a582e3697))

# [1.9.0](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/compare/v1.8.0...v1.9.0) (2023-10-04)


### Bug Fixes

* **build:** Add semantic release libs ([d00e8b2](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/d00e8b2425ac189012257e1963dfe36ca1d7b901))
* **build:** Add semantic-release script ([c28e419](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/c28e419be3ee839bd052aa28e1d32ca42f6498c2))
* **build:** update version ([251f9fa](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/251f9faf781fbff9121eeb6bb5d85242c0deef53))
* **deps:** align deps with stripes ([009b218](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/009b218e693677e0c0e5a494091086dca9855b5d))
* **deps:** update stripes-ill deps to align with stripes core ([f4659d1](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/f4659d1eb0531761a8447d893937407ae68f138b))
* **deps:** Working on dependencies ([f1a623d](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/f1a623d292087ca4b1bb8f045b50bbef3ce3509f))
* **deps:** Working on dependencies ([224cde9](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/224cde97275108c6dbbcb6847b573b2f0d7472c0))
* **deps:** Working on dependencies ([d56c59e](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/d56c59e4629e483072abfac84bcc931c05e77c2d))
* **deps:** Working on dependencies ([081a79c](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/081a79c1c45a5ffb95e68d89d1cf2821f8b2828a))


### Features

* Combined actions ([dd2d703](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/dd2d703b435650c21fac1f58272a34e6b98a51f9))
* Delete failure callout ([2fd71f3](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/2fd71f353f069c992898f3453ecbc8842a2536f4))
* excludeActions ([9dbf20a](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/9dbf20ab8e1f1b810c1e125828519b3f224bcd09))
* HostLMSLocations ([3ffd1a1](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/3ffd1a1527fe3948d9225d3274d762f4ea8fb542))
* Name editing, code viewing ([7915b8d](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/7915b8de976619ae9831cc095440a51f80403bd4))
* PickupLocation default ([2a73ea0](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/2a73ea03debd1e26dc7386f252f61057ace3f346))
* SupplierCheckInToReshareAndSupplierMarkShipped ([e84427f](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/e84427f01b691c4b9e6f3699a844fc1dff5f1978))

# [1.9.0](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/compare/v1.8.0...v1.9.0) (2023-10-04)


### Bug Fixes

* **build:** Add semantic release libs ([d00e8b2](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/d00e8b2425ac189012257e1963dfe36ca1d7b901))
* **build:** Add semantic-release script ([c28e419](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/c28e419be3ee839bd052aa28e1d32ca42f6498c2))
* **deps:** align deps with stripes ([009b218](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/009b218e693677e0c0e5a494091086dca9855b5d))
* **deps:** update stripes-ill deps to align with stripes core ([f4659d1](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/f4659d1eb0531761a8447d893937407ae68f138b))
* **deps:** Working on dependencies ([f1a623d](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/f1a623d292087ca4b1bb8f045b50bbef3ce3509f))
* **deps:** Working on dependencies ([224cde9](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/224cde97275108c6dbbcb6847b573b2f0d7472c0))
* **deps:** Working on dependencies ([d56c59e](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/d56c59e4629e483072abfac84bcc931c05e77c2d))
* **deps:** Working on dependencies ([081a79c](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/081a79c1c45a5ffb95e68d89d1cf2821f8b2828a))


### Features

* Combined actions ([dd2d703](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/dd2d703b435650c21fac1f58272a34e6b98a51f9))
* Delete failure callout ([2fd71f3](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/2fd71f353f069c992898f3453ecbc8842a2536f4))
* excludeActions ([9dbf20a](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/9dbf20ab8e1f1b810c1e125828519b3f224bcd09))
* HostLMSLocations ([3ffd1a1](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/3ffd1a1527fe3948d9225d3274d762f4ea8fb542))
* Name editing, code viewing ([7915b8d](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/7915b8de976619ae9831cc095440a51f80403bd4))
* PickupLocation default ([2a73ea0](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/2a73ea03debd1e26dc7386f252f61057ace3f346))
* SupplierCheckInToReshareAndSupplierMarkShipped ([e84427f](https://gitlab.com/knowledge-integration/libraries/networks/ill-ui/commit/e84427f01b691c4b9e6f3699a844fc1dff5f1978))

# Change history for ui-ill

## [1.2.0](https://gitlab.com/knowledge-integration/libraries/networks/ui-ill/tree/v1.2.0) (IN PROGRESS)

* Display item barcode in Request app search results. Fixes PR-819.
* Add search by item barcode to Request app. Fixes PR-820.
* Partial support for settings help in translations file. Towards PR-828.
* Set up permissions for settings pages. Fixes PR-813. XXX NOT COMPLETE.
* Fix legend-alignment problems in pull-slip notification editor. Fixes PR-723.
* Layout improvements to pull slip delivery settings screen. Fixes PR-843.
* Display due date on Requester view. Fixes PR-853.
* Add a dummy "none" option to list of LMS locations when editing a pull-slip notification schedule. Fixes PR-847.
* Rename "Chat" to "Messages" in helper app. Fixes PR-991.
* Remove _all_ translations from `en_US.json`, which is not presently maintained and had become and accidental dumping ground for translations intended for `en.json`. Fixes PR-996.
* Format due dates according to locale. Fixes PR-1003.

## [1.1.0](https://gitlab.com/knowledge-integration/libraries/networks/ui-ill/tree/v1.1.0) (2020-08-24)

* Initial release
* Support "sub-applications" such as `ui-supply` by the top-level component determining the app-name from the route and passing it as the `appName` prop to the top-level and settings components.
* Support creation of new patron-request records from the UI (PR-264).

## 1.0.0 (NEVER RELEASED)

* New app created with stripes-cli
